<?php

class ApiFacturacionController extends Controller {

    // Members
    /**
     * Key which has to be in HTTP USERNAME and PASSWORD headers
     */
    const APPLICATION_ID = 'ASCCPE';

    /**
     * Default response format
     * either 'json' or 'xml'
     */
    private $format = 'json';

    /**
     * @return array action filters
     */
    public function filters() {
        return array();
    }

    // Actions
    public function actionList() {

        // Get the respective model instance
        switch ($_GET['model']) {
            case 'posts':
                $models = Post::model()->findAll();
                break;
            case "sendCFDIPG":
                $this->actionSendCFDIPG();
                break;
            default:
                // Model not implemented error
                $this->_sendResponse(501, sprintf(
                                'Error: Mode <b>list</b> is not implemented for model <b>%s</b>', $_GET['model']));
                Yii::app()->end();
        }
        // Did we get some results?
        if (empty($models)) {
            // No
            $this->_sendResponse(200, sprintf('No items where found for model <b>%s</b>', $_GET['model']));
        } else {
            // Prepare response
            $rows = array();
            foreach ($models as $model)
                $rows[] = $model->attributes;
            // Send the response
            $this->_sendResponse(200, CJSON::encode($rows));
        }
    }




    public function actionSendCFDIPG() {


        // Parse the PUT parameters. This didn't work: parse_str(file_get_contents('php://input'), $put_vars);
        $json = file_get_contents('php://input'); //$GLOBALS['HTTP_RAW_POST_DATA'] is not preferred: http://www.php.net/manual/en/ini.core.php#ini.always-populate-raw-post-data
        $put_vars = CJSON::decode($json, true);
        $this->_checkAuth($put_vars["tokenKeyApp"]);
        $status=200;
        $response="";
        $nuevo=new FTFACTG();
        if (!empty($put_vars)) {//RECIBE LAS VARIABLES POR POST DEL EXTERIOR


            $nuevo->attributes=$put_vars["CFDI"]["FTFACTG"];
            $idCliente=$nuevo->MNUMCTE;
            $Cliente=DACLIEN::model()->find("DNUMCTE=:id",array(":id" => ($idCliente*1)));
            if(!empty($Cliente)){
                /**
                 * VALIDA QUE LOS CODIGOS DE PRODUCTO EXISTAN EN EL SISTEMA ANTES DE
                 * ENVIAR LA FACTURACION COMPLETA
                 */
                $Paridas=$put_vars["CFDI"]["FTFACTD"];
                $bolProcesaCFDi=true;
                if(count($Paridas)>0){
                    $final="";
                    foreach ($Paridas as $parida){
                        $criteriaProducto=new CDbCriteria();
                        if(!empty($parida["noReferencia"])){
                            $criteriaProducto->compare("ACODPRO",$parida["noReferencia"]);
                            $Producto=IBINVEN::model()->count($criteriaProducto);
                            if($Producto==0){
                                $bolProcesaCFDi=false;
                                break;
                            }
                            $final.= "(".$parida["noReferencia"]."[".$Producto."]) --> ";
                        }else{
                            $final.= "(".$parida["noReferencia"]."[----]) --> ";
                            $bolProcesaCFDi=false;
                            break;
                        }

                    }
                   /* $status=500;
                    $resultado=array(
                        "resultado" => $final." proceso final ".$bolProcesaCFDi,


                    );
                    $response=serialize($resultado);
                    $this->_sendResponse($status,$response);
                    die;*/

                    if($bolProcesaCFDi==true){
                        $nuevo->MRAZON=$Cliente->DRAZON;
                        $nuevo->MCONPAG=$Cliente->DCONPAG;
                        $nuevo->MCALLE=$Cliente->DCALLE;
                        $nuevo->MUCFDI=(empty($Cliente->DUCFDI)?"P01":$Cliente->DUCFDI);
                        $nuevo->MMETPAG='PUE';
                        $nuevo->MNUMEXT=$Cliente->DNUMEXT;
                        $nuevo->MNUMINT=(empty($Cliente->DNUMINT)?" ":$Cliente->DNUMINT);
                        $nuevo->MDELMUN=(empty($Cliente->DDELMUN)?" ":$Cliente->DDELMUN);
                        $nuevo->MCIUDAD=$Cliente->DCIUDAD;
                        $nuevo->MCODPOS=$Cliente->DCODPOS;
                        $nuevo->MREFEREN=(empty($Cliente->DREFEREN)?" ":$Cliente->DREFEREN);
                        $nuevo->MCOLON=$Cliente->DCOLON;
                        $nuevo->MCODEST=$Cliente->DCODEST;
                        $nuevo->MTIPOPAGO="01";
                        $nuevo->MAGENTE=$Cliente->DAGENTE;
                        $nuevo->MDESCTE=(empty($nuevo->MDESCTE)?"0.00":$nuevo->MDESCTE);
                        $nuevo->MDESCT2=(empty($nuevo->MDESCT2)?"0.00":$nuevo->MDESCT2);
                        $nuevo->MDESCT3=(empty($nuevo->MDESCT3)?"0.00":$nuevo->MDESCT3);
                        $nuevo->MCVEIEPS=(empty($nuevo->MCVEIEPS)?"0.00":$nuevo->MCVEIEPS);
                        $nuevo->MCVEIVAR=(empty($nuevo->MCVEIVAR)?"0.00":$nuevo->MCVEIVAR);
                        $nuevo->MCVEMON=(empty($nuevo->MCVEMON)?"MN":$nuevo->MCVEMON);
                        $nuevo->MTIPOCA=(empty($nuevo->MTIPOCA)?"1.000":$nuevo->MTIPOCA);
                        $nuevo->MPESON=(empty($nuevo->MPESON)?"0.000":$nuevo->MPESON);
                        $nuevo->MPESOB=(empty($nuevo->MPESOB)?"0.000":$nuevo->MPESOB);
                        $nuevo->MPESOB=(empty($nuevo->MPESOB)?"0.000":$nuevo->MPESOB);
                        $nuevo->MIMPFLE=(empty($nuevo->MIMPFLE)?"0.00":$nuevo->MIMPFLE);
                        $nuevo->MIMPOTR=(empty($nuevo->MIMPOTR)?"0.00":$nuevo->MIMPOTR);
                        $nuevo->MPREESP=(empty($nuevo->MPREESP)?"0":$nuevo->MPREESP);
                        $nuevo->MIVAINC=(empty($nuevo->MIVAINC)?"0":$nuevo->MIVAINC);
                        $nuevo->MIEPSINC=(empty($nuevo->MIEPSINC)?"0":$nuevo->MIEPSINC);
                        $nuevo->MIVAREN=(empty($nuevo->MIVAREN)?"1":$nuevo->MIVAREN);
                        $nuevo->MIMPDES=(empty($nuevo->MIMPDES)?"0.00":$nuevo->MIMPDES);
                        $nuevo->MIMPIVAR=(empty($nuevo->MIMPIVAR)?"0.00":$nuevo->MIMPIVAR);
                        $nuevo->MIMPISRR=(empty($nuevo->MIMPISRR)?"0.00":$nuevo->MIMPISRR);
                        $nuevo->MIMPIEPS=(empty($nuevo->MIMPIEPS)?"0.00":$nuevo->MIMPIEPS);
                        $nuevo->MCOMAGE=(empty($nuevo->MCOMAGE)?"0.00":$nuevo->MCOMAGE);
                        $nuevo->MDESREN=(empty($nuevo->MDESREN)?"0.00":$nuevo->MDESREN);
                        $nuevo->MIMPCOS=(empty($nuevo->MIMPCOS)?"0.00":$nuevo->MIMPCOS);
                        $nuevo->MPARCIAL=(empty($nuevo->MPARCIAL)?"0":$nuevo->MPARCIAL);
                        $nuevo->MCVEPEDIM=(empty($nuevo->MCVEPEDIM)?" ":$nuevo->MCVEPEDIM);
                        $nuevo->MCERORI=(empty($nuevo->MCERORI)?"0":$nuevo->MCERORI);
                        $nuevo->MOBSEXP=(empty($nuevo->MOBSEXP)?" ":$nuevo->MOBSEXP);
                        $nuevo->MINICIA=(empty($nuevo->MINICIA)?"AMS":$nuevo->MINICIA);
                        $nuevo->MPORLOC=(empty($nuevo->MPORLOC)?"0.00":$nuevo->MPORLOC);
                        $nuevo->MLOCINC=(empty($nuevo->MLOCINC)?"0":$nuevo->MLOCINC);
                        $nuevo->MPORLOCR=(empty($nuevo->MPORLOCR)?"0.00":$nuevo->MPORLOCR);
                        $nuevo->MIMPIEPSC=(empty($nuevo->MIMPIEPSC)?"0.00":$nuevo->MIMPIEPSC);
                        $nuevo->MIMPLOC=(empty($nuevo->MIMPLOC)?"0.00":$nuevo->MIMPLOC);
                        $nuevo->MIMPLOCR=(empty($nuevo->MIMPLOCR)?"0.00":$nuevo->MIMPLOCR);
                        $nuevo->MNOREP=(empty($nuevo->MNOREP)?"0":$nuevo->MNOREP);


                        $criteriaFolio=new CDbCriteria();
                        $criteriaFolio->compare("PNUMERO",$nuevo->MCVEMOV);
                        $folioUpdate=SACLAVE::model()->find($criteriaFolio);

                        $nuevo->MIEPSREN=$folioUpdate->PIEPSREN;
                        $nuevo->MVIGIM=$folioUpdate->PVIGIM;
                        $nuevo->MLOCREN=$folioUpdate->PLOCREN;
                        $nuevo->MIVARENR=$folioUpdate->PIVARENR;


                        $nuevo->MEMBARQ=(empty($nuevo->MEMBARQ)?" ":$nuevo->MEMBARQ);
                        $nuevo->MPEDCTE=(empty($nuevo->MPEDCTE)?" ":$nuevo->MPEDCTE);
                        $nuevo->MFLETE=(empty($nuevo->MFLETE)?" ":$nuevo->MFLETE);
                        $nuevo->MLAB=(empty($nuevo->MLAB)?" ":$nuevo->MLAB);
                        $nuevo->MGLN=(empty($nuevo->MGLN)?" ":$nuevo->MGLN);
                        $nuevo->MBAJA=0;
                        $nuevo->MHORAM=date("H:i:s");
                        $nuevo->MFECHAR=date("Y-m-d");
                        $nuevo->MENVIADO=0;
                        //SE EXTRAE EL PROXIMO FOLIO
                        $criteriaFolio=new CDbCriteria();
                        $criteriaFolio->compare("MCVEMOV",$nuevo->MCVEMOV);
                        $criteriaFolio->select="max(MFOLIOG)+1 AS 'MFOLIOG'";
                        $folio=FTFACTG::model()->find($criteriaFolio);

                        if(!empty($folio)){
                            $nuevo->MFOLIOG=$folio->MFOLIOG;
                        }else{
                            $nuevo->MFOLIOG=0;
                        }

                        if($nuevo->save()){
                            /**
                             * SI GUARDA EL ENCABEZADO PASA A REGISTRAR LAS PARTIDAS
                             */


                            $paridatest="--";
                            foreach ($Paridas as $parida){
                                $NuevaPartida=new FTFACTD();
                                $importeConImpoesto=$parida["cantidad"]*$parida["precioUnitario"];
                                $importeNeto=($importeConImpoesto)/(1+($parida["tasa"]/100));
                                $NuevaPartida->MCANTID=$parida["cantidad"];
                                $NuevaPartida->MDESCR=$parida["descProducto"];
                                $criteriaGrupo=new CDbCriteria();
                                $criteriaGrupo->compare("ACODPRO",$parida["noReferencia"]);
                                $criteriaGrupo->limit=1;
                                $Grupo=IBINVEN::model()->find($criteriaGrupo);
                                if(!empty($Grupo)){
                                    $NuevaPartida->MGRUPO=$Grupo->AGRUPO;
                                    $NuevaPartida->MUNIMED=$Grupo->AUNIME;
                                }else{
                                    $NuevaPartida->MGRUPO="GRAL";
                                    $NuevaPartida->MUNIMED="PZA";
                                }
                                $NuevaPartida->MIMPNETO=$importeNeto;
                                $NuevaPartida->MIMPORT=$importeNeto;
                                $NuevaPartida->MIMPIVA=$importeConImpoesto-$importeNeto;
                                $NuevaPartida->MIMPIVAR=(empty($NuevaPartida->MIMPIVAR)?"0.00":$NuevaPartida->MIMPIVAR);
                                $NuevaPartida->MIMPISRR=(empty($NuevaPartida->MIMPISRR)?"0.00":$NuevaPartida->MIMPISRR);
                                $NuevaPartida->MIMPDESR=(empty($NuevaPartida->MIMPDESR)?"0.00":$NuevaPartida->MIMPDESR);
                                $NuevaPartida->MCVEMOV=$nuevo->MCVEMOV;
                                $NuevaPartida->MFOLIOG=$nuevo->MFOLIOG;
                                $NuevaPartida->MALMACE=$nuevo->MALMACE;
                                $NuevaPartida->MPRECIO=$parida["precioUnitario"];
                                $NuevaPartida->MCODPRO=$parida["noReferencia"];
                                $NuevaPartida->MPORIVA=number_format($parida["tasa"],0);
                                $NuevaPartida->MPORIVAR="0.00";
                                $NuevaPartida->MPORLOC="0.00";
                                $NuevaPartida->MPORLOCR="0.00";
                                $NuevaPartida->MPORIEPS="0.00";
                                $NuevaPartida->MIMPIEPS="0.00";
                                $NuevaPartida->MCUOIEPS="0.00";
                                $NuevaPartida->MIMPIEPSC="0.00";
                                $NuevaPartida->MPORIEPSG="0.00";
                                $NuevaPartida->MIMPDE1=(empty($NuevaPartida->MIMPDE1)?"0.00":$NuevaPartida->MIMPDE1);
                                $NuevaPartida->MIMPDE2=(empty($NuevaPartida->MIMPDE2)?"0.00":$NuevaPartida->MIMPDE2);
                                $NuevaPartida->MIMPDE3=(empty($NuevaPartida->MIMPDE3)?"0.00":$NuevaPartida->MIMPDE3);
                                $NuevaPartida->MPORDES="0.00";
                                $NuevaPartida->MEXEIVA="0";
                                $NuevaPartida->MIMPCOS=(empty($NuevaPartida->MIMPCOS)?"0.00":$NuevaPartida->MIMPCOS);//caso por revisar para calcular el costeo

                                $NuevaPartida->MNUMREN=str_pad($parida["numRenglon"], 4, "0", STR_PAD_LEFT);
                                if($NuevaPartida->save()){
                                    $status=200;
                                    $folioUpdate->PFOLIO=$folio->MFOLIOG+1;
                                    $folioUpdate->update();
                                    $resultado=array(
                                        "folio" => $nuevo->MFOLIOG,
                                        "serie" => $nuevo->MCVEMOV,
                                        "total" => $importeConImpoesto,

                                    );
                                    $response=serialize($resultado);
                                }else{
                                    $status=500;
                                    $response=$NuevaPartida->getErrors();
                                }
                            }

                        }else{
                            $status=500;
                            $response=$nuevo->getErrors();
                        }
                    }else{
                        $status=500;
                        $nuevo->addError("MNUMCTE", "Uno o varios Items del CFDI no se encuentran referenciados en Esensial");
                        $response=$nuevo->getErrors();
                    }
                }else{
                    $status=500;
                    $nuevo->addError("MNUMCTE", "El Documento no contiene items para procesar");
                    $response=$nuevo->getErrors();
                }



            }else{
                $status=500;
                $nuevo->addError("MNUMCTE", "El Cliente ".$nuevo->MNUMCTE." No existe!");
                $response=$nuevo->getErrors();
            }
        } else {
            $status=500;
            $nuevo->addError("MNUMCTE", "NO SE RECIBIO INFORMACION PARA PROCESAR");
            $response=$nuevo->getErrors();

        }

        if(!empty($response)){
            $this->_sendResponse($status,$response);
        }else{
            $this->_sendResponse($status);
        }


    }



    public function actions() {
        // return external action classes, e.g.:
        return array(
            'action1' => 'path.to.ActionClass',
            'action2' => array(
                'class' => 'path.to.AnotherActionClass',
                'propertyName' => 'propertyValue',
            ),
        );
    }

    private function _sendResponse($status = 200, $body = '', $content_type = 'text/html') {
        // set the status
        $status_header = 'HTTP/1.1 ' . $status . ' ' . $this->_getStatusCodeMessage($status);
        header($status_header);
        // and the content type
        //header('Content-type: ' . $content_type);
        header('content-type: application/json; charset=utf-8');
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');
        // pages with body are easy
        $response=array();
        if ($body != '') {
            // send the body
            $message= $body;
        } // we need to create the body if none is passed
        else {
            // create some body messages
            $message = '';

            // this is purely optional, but makes the pages a little nicer to read
            // for your users.  Since you won't likely send a lot of different status codes,
            // this also shouldn't be too ponderous to maintain
            $nuevo=new FTFACTG();
            switch ($status) {
                case 401:

                    $messageText = 'You must be authorized to view this page.';
                    $nuevo->addError("MNUMCTE", $messageText);

                    break;
                case 404:
                    $messageText = 'The requested URL ' . $_SERVER['REQUEST_URI'] . ' was not found.';
                    $nuevo->addError("MNUMCTE", $messageText);
                    break;
                case 500:
                    $messageText = 'The server encountered an error processing your request.';
                    $nuevo->addError("MNUMCTE", $messageText);
                    break;
                case 501:
                    $messageText = 'The requested method is not implemented.';
                    $nuevo->addError("MNUMCTE", $messageText);
                    break;
            }
            $message=$nuevo->getErrors();

            // servers don't always have a signature turned on
            // (this is an apache directive "ServerSignature On")
            $signature = ($_SERVER['SERVER_SIGNATURE'] == '') ? $_SERVER['SERVER_SOFTWARE'] . ' Server at ' . $_SERVER['SERVER_NAME'] . ' Port ' . $_SERVER['SERVER_PORT'] : $_SERVER['SERVER_SIGNATURE'];



        }
        $response["response"]=$message;
        $response["status"]=$status;
        echo CJSON::encode($response);
        Yii::app()->end();
    }

    private function _getStatusCodeMessage($status) {
        // these could be stored in a .ini file and loaded
        // via parse_ini_file()... however, this will suffice
        // for an example
        $codes = array(
            200 => 'OK',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
        );
        return (isset($codes[$status])) ? $codes[$status] : '';
    }

    private function _checkAuth($tokenProcess) {
        // Check if we have the USERNAME and PASSWORD HTTP headers set?

        if (!(isset($tokenProcess))) {
            // Error: Unauthorized
            $this->_sendResponse(401);

        }
        if ($tokenProcess != "5acd20dc3966648833cc1d1b2d47bddf76d50e3e") {
            // Error: Unauthorized
            $this->_sendResponse(401);
        }
    }


}
