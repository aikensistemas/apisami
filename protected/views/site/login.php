<!-- Container-fluid starts -->
<div class="container">
        <!-- Authentication card start -->
        <div class="login-card card-block ">
            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'login-form',
                'enableClientValidation' => true,
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                ),

            ));
            ?>
            <div class="row">
                <div class="col-12">
                    <span class="" style="font-size: 20px;">
                        Bienvenido!
                    </span>
                        <br>
                        <span class="card-title text-muted">
                            Identíficate para continuar
                    </span>
                </div>
            </div>
            <?php
            if(!empty($model->errors)){
                ?>
                <div class="row mt-3">

                      <div class="col-12" >
                          <div class="alert border-danger " role="alert" style="padding-left: 7px; padding-right: 7px; ">
                              <div class="row">
                                  <div class="col-1 bg-danger" style="text-align: center; ">
                                      <i class="fa fa-exclamation text-white" aria-hidden="true" style="margin-top: 50%; margin-bottom: 50%;"></i>
                                  </div>
                                  <div class="col-11 " style="padding: 20px; background-color: lightpink  " >
                                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                      <span style="font-size: 12px; ; color: darkred" ><?php
                                          $strError=$form->errorSummary($model);
                                          $strError=str_replace("p>","strong>",$strError);
                                          echo $strError;
                                          ?></span>
                                  </div>
                              </div>
                          </div>
                      </div>

                </div>
            <?php
            }
            ?>



            <div class="row">
                <div class="col-12 mt-4">
                    <div class="wrap-input100 ">
                        <?php echo $form->textField($model,
                            'username',
                            array('class' => "input100 form-control ",'autocomplete' => 'off', 'placeholder' => "Nombre de Usuario", 'value' => $model->username));
                        ?>
                        <span class="focus-input100"></span>
                        <span class="symbol-input100"><i class="zmdi zmdi-account" aria-hidden="true"></i></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 mt-1">
                    <div class="wrap-input100 ">
                        <?php echo $form->passwordField($model,
                            'password',
                            array('class' => "input100 form-control ",'autocomplete' => 'off', 'placeholder' => "Contraseña", 'value' => $model->password));
                        ?>
                        <span class="focus-input100"></span>
                        <span class="symbol-input100"><i class="zmdi zmdi-lock" aria-hidden="true"></i></span>
                    </div>
                </div>


            </div>
            <div class="row">
                <div class="col-12 mt-1">
                    <div class="container-login100-form-btn">
                        <?php echo CHtml::submitButton('Ingresar', array('class' => 'btn btn-cyan btn-block', 'style' => '')); ?>
                    </div>
                </div>

            </div>

            <?php $this->endWidget(); ?>

            <div class="row " >
                <div class="col-12 text-right mt-5">
                    <img src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/images/bg/logo.png" class="header-brand-img" alt="logo.png">

                </div>
            </div>

        </div>
    <!-- end of row -->
</div>
<!-- end of container-fluid -->
