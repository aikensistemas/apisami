<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
    <!--favicon -->
    <body class="app">
    <div id="content">
        <!-- GLOBAL-LOADER -->
        <div class="page">
            <div class="page-main">
                <?php echo $content; ?>
            </div>
        </div>

    </div><!-- content -->
    </body>
<?php $this->endContent(); ?>