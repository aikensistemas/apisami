<!doctype html>
<html lang="en" dir="ltr">
<head>
<!--favicon -->
<link rel="icon" href="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/images/brand/favicon.ico" type="image/x-icon"/>

<!-- TITLE -->
<title>LISA | Logistic Internal System Administrator </title>
    <!-- JQUERY SCRIPTS JS-->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/js/vendors/jquery-3.2.1.min.js"></script>



    <link href="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/css/style.css" rel="stylesheet"/>
<link href="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/css/color-style.css" rel="stylesheet"/>
<link href="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/css/skins-modes.css" rel="stylesheet"/>

<!--SIDEMENU CSS-->
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/plugins/sidemenu/sidemenu.css">

<!--SIDEMENU-RESPONSIVE-TABS  css -->
<link href="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/plugins/sidemenu-responsive-tabs/css/sidemenu-responsive-tabs.css" rel="stylesheet">

<!--C3 CHARTS CSS -->
<link href="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/plugins/charts-c3/c3-chart.css" rel="stylesheet"/>

<!-- TABS CSS -->
<link href="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/plugins/tabs/tabs-2.css" rel="stylesheet" type="text/css">

<!-- P-SCROll CSS -->
<link href="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/plugins/p-scroll/p-scroll.css" rel="stylesheet" type="text/css">

<!--- FONT-ICONS CSS -->
<link href="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/css/icons.css" rel="stylesheet"/>

    <!--C3 CHARTS CSS -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/plugins/charts-c3/c3-chart.css" rel="stylesheet"/>

<!-- SIDEBAR CSS -->
<link href="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/plugins/right-sidebar/right-sidebar.css" rel="stylesheet">

    <!-- Data table css -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/plugins/datatable/css/dataTables.bootstrap4.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/plugins/Datatable/css/buttons.bootstrap4.min.css">
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/plugins/datatable/responsive.bootstrap4.min.css" rel="stylesheet" />

    <!--Sweat Alert Css-->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/plugins/sweet-alert/sweetalert.css" rel="stylesheet" />
    <!-- JQUERY SCRIPTS JS-->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/js/vendors/jquery-3.2.1.min.js"></script>

</head>

<body class="app sidebar-mini sidenav-toggled" id="body">
<!-- GLOBAL-LOADER -->
<div id="global-loader">
    <img src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/images/svgs/loader.svg" class="loader-img" alt="Loader">
</div>
<div class="page">
    <div class="page-main">
        <!-- HEADER -->
        <div class="header app-header">
            <div class="container-fluid">
                <div class="d-flex header-nav">
                    <div class="color-headerlogo">
                        <a class="header-desktop"  style="background-image: url('<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/images/bg/logoNav.png'); "></a>
                        <a class="header-mobile" href="index.html"></a>
                    </div><!-- Color LOGO -->
                    <a href="#" data-toggle="sidebar" class="nav-link icon toggle"><i class="fe fe-align-justify"></i></a>
                    <div class="d-flex order-lg-2 ml-auto header-right-icons header-search-icon">
                        <div class="dropdown  header-fullscreen">
                            <a class="nav-link icon full-screen-link nav-link-bg" id="fullscreen-button">
                                <i class="fe fe-minimize" ></i>
                            </a>
                        </div><!-- FULL-SCREEN -->
                        <div class="dropdown  notifications">
                            <a class="nav-link icon" data-toggle="dropdown">
                                <i class="fe fe-bell"></i>
                                <span class="pulse bg-danger"></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                <a href="#" class="dropdown-item mt-2 d-flex pb-3">
                                    <div class="notifyimg bg-info">
                                        <i class="fa fa-thumbs-o-up"></i>
                                    </div>
                                    <div>
                                        <h6 class="mb-1">Someone likes our posts.</h6>
                                        <div class="small text-muted">3 hours ago</div>
                                    </div>
                                </a>
                                <div class="border-top">
                                    <a href="#" class="dropdown-item text-center">View all Notification</a>
                                </div>
                            </div>
                        </div><!-- NOTIFICATIONS -->
                        <div class="dropdown  message">
                            <a class="nav-link icon text-center" data-toggle="dropdown">
                                <i class="fe fe-mail"></i>
                                <span class=" nav-unread badge badge-warning badge-pill">6</span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                <a href="#" class="dropdown-item d-flex mt-2 pb-3">
                                    <div class="avatar avatar-md brround mr-3 d-block cover-image" data-image-src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/images/users/male/4.jpg">
                                        <span class="avatar-status bg-green"></span>
                                    </div>
                                    <div>
                                        <h6 class="mb-1">Lucas Walsh</h6>
                                        <p class="mb-0 fs-13 ">Hey! there I' am available</p>
                                        <div class="small text-muted">3 hours ago</div>
                                    </div>
                                </a>
                                <div class="border-top">
                                    <a href="#" class="dropdown-item text-center">See all Messages</a>
                                </div>
                            </div>
                        </div><!-- MESSAGE-BOX -->
                        <div class="dropdown header-user">
                            <a href="#" class="nav-link icon" data-toggle="dropdown">
                                <span><img src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/images/users/male/2.jpg" alt="profile-user" class="avatar brround cover-image mb-0 ml-0"></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                <div class=" dropdown-header noti-title text-center border-bottom p-3">
                                    <div class="header-usertext">
                                        <h5 class="mb-1"><?php echo ucwords(strtolower(Yii::app()->user->nombre)); ?></h5>
                                        <p class="mb-0">Web Developer</p>
                                    </div>
                                </div>
                                <a class="dropdown-item" href="profile.html">
                                    <i class="mdi mdi-account-outline mr-2"></i> <span>My profile</span>
                                </a>
                                <a class="dropdown-item" href="#">
                                    <i class="mdi mdi-settings mr-2"></i> <span>Configuraciones</span>
                                </a>
                                <a class="dropdown-item" href="#">
                                    <i class="fe fe-calendar mr-2"></i> <span>Actividad reciente</span>
                                </a>
                                <a class="dropdown-item" href="#">
                                    <i class="mdi mdi-compass-outline mr-2"></i> <span>Soporte</span>
                                </a>
                                <a class="dropdown-item" href="<?php echo CController::createUrl('site/logout') ?>">
                                    <i class="mdi  mdi-logout-variant mr-2"></i> <span>Salir</span>
                                </a>
                            </div>
                        </div><!-- SIDE-MENU -->
                        <div class="dropdown  header-fullscreen">
                            <a class="nav-link icon sidebar-right-mobile" data-toggle="sidebar-right" data-target=".sidebar-right">
                                <i class="fe fe-align-right" ></i>
                            </a>
                        </div><!-- Side menu -->
                    </div>
                </div>
            </div>
        </div>
        <!-- HEADER END -->

        <!-- Sidebar menu-->
        <div class="app-sidebar__overlay " data-toggle="sidebar"></div>
        <div class="container">
        <aside class="app-sidebar" >
            <div class="side-tab-body p-0 border-1" id="sidemenu-Tab" >
                <div class="first-sidemenu bg-cyan" >
                    <ul class="resp-tabs-list hor_1 bg-cyan">
                        <?php
                        $criterio=new CDbCriteria;
                        $criterio->condition='idCatperfil=:idCatperfil';
                        $criterio->params=array(':idCatperfil'=>Yii::app()->user->idCatPerfil);
                        $criterio->order='menu';
                        $criterio->group='menu';
                        $menus=Viewmenu::model()->findAll($criterio);
                        foreach ($menus as $menu){
                          ?>
                            <li class="resp-tab-active active   "><i class="side-menu__icon <?=$menu->iconoMenu ?>" style="font-size: 25px;"></i><span class="side-menu__label"><p>
                                      <?=ucwords(strtolower($menu->menu)) ?></span>
                            </li>
                        <?php
                        }
                        ?>
                    </ul>
                </div>

                <div class="second-sidemenu">
                    <div class="resp-tabs-container hor_1">
                        <?php
                        $criterio=new CDbCriteria;
                        $criterio->condition='idCatperfil=:idCatperfil';
                        $criterio->params=array(':idCatperfil'=>Yii::app()->user->idCatPerfil);
                        $criterio->group='idCatMenu';
                        $criterio->order='menu';
                        $menus=Viewmenu::model()->findAll($criterio);

                        foreach ($menus as $menu){
                            ?>
                            <div class="resp-tab-content-active">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="panel sidetab-menu">
                                            <div class="side-menu p-0">
                                                <div class="slide submenu">
                                                    <div class="row">
                                                        <h5 class="mt-3 mb-4 col-6" ><?=ucwords(strtolower($menu->menu)) ?></h5>
                                                        <div class="col-6">
                                                            <a href="#" data-toggle="sidebar" class="nav-link icon toggle pull-right">
                                                                <i class="fa fa-angle-double-left"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <ul class="side-account">
                                                        <?php
                                                        $criteri1=new CDbCriteria;
                                                        $criteri1->condition='idCatperfil=:idCatperfil';
                                                        $criteri1->params=array(':idCatperfil'=>Yii::app()->user->idCatPerfil);
                                                        $criteri1->compare("idCatmenu",$menu->idCatmenu);
                                                        $criteri1->order='menu';
                                                        $Submenus=Viewmenu::model()->findAll($criteri1);
                                                        foreach ($Submenus as $submenu) {
                                                            ?>
                                                            <li class="acc-link" >
                                                                <a href="<?php echo CController::createUrl($submenu->link)?>">
                                                                    <i class="<?=$submenu->iconoBoton ?> text-<?=$submenu->color ?> mr-2 fs-20"></i><?=ucfirst(strtolower($submenu->boton)) ?>
                                                                </a>
                                                            </li>
                                                            <?php
                                                        }
                                                        ?>



                                                    </ul>



                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <?php
                        }
                        ?>




                    </div>
                </div>
            </div>
        </aside>
        </div>
        <!--sidemenu end-->

        <!-- CONTAINER -->
        <div class="app-content">
            <div class="section">
                <!-- PAGE-HEADER END -->
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12">
                        <?php echo $content; ?>
                    </div>
                </div>
            <!-- END ROW-->

            </div>
            <!-- CONTAINER END -->
        </div>
    </div>

    <!-- Right-sidebar-->
    <div class="sidebar sidebar-right sidebar-animate">
        <div class="p-2 pr-3 mb-2 sidebar-icon">
            <a href="#" class="text-right float-right" data-toggle="sidebar-right" data-target=".sidebar-right"><i class="fe fe-x"></i></a>
        </div>
        <div class="tab-menu-heading siderbar-tabs border-0">
            <div class="tabs-menu ">
                <!-- Tabs -->
                <ul class="nav panel-tabs">
                    <li class=""><a href="#tab1"  class="active" data-toggle="tab">Configuraciones</a></li>
                </ul>
            </div>
        </div>
        <div class="panel-body tabs-menu-body side-tab-body p-0 border-0 ">
            <div class="tab-content border-top">
                <div class="tab-pane active " id="tab1">
                    <div class="p-3 border-bottom">
                        <h5 class="border-bottom-0 mb-0">Configuraciones generales</h5>
                    </div>
                    <div class="p-4">
                        <div class="switch-settings">
                            <div class="d-flex mb-2">
                                <span class="mr-auto fs-15">Noficiaciones</span>
                                <div class="onoffswitch2">
                                    <input type="checkbox" name="onoffswitch2" id="onoffswitch" class="onoffswitch2-checkbox" checked>
                                    <label for="onoffswitch" class="onoffswitch2-label"></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="p-3 border-bottom">
                        <h5 class="border-bottom-0 mb-0">Overview</h5>
                    </div>
                    <div class="p-4">
                        <div class="progress-wrapper">
                            <div class="mb-3">
                                <p class="mb-2">Achieves<span class="float-right text-muted font-weight-normal">80%</span></p>
                                <div class="progress h-1">
                                    <div class="progress-bar bg-primary w-80 " role="progressbar"></div>
                                </div>
                            </div>
                        </div>
                        <div class="progress-wrapper pt-2">
                            <div class="mb-3">
                                <p class="mb-2">Projects<span class="float-right text-muted font-weight-normal">60%</span></p>
                                <div class="progress h-1">
                                    <div class="progress-bar bg-secondary w-60 " role="progressbar"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tab2">
                    <div class="list-group-item d-flex  align-items-center border-top-0">
                        <div class="mr-2">
                            <span class="avatar avatar-md brround cover-image" data-image-src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/images/users/female/2.jpg" style="background: url(&quot;<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/images/users/female/2.jpg&quot;) center center;"></span>
                        </div>
                        <div class="">
                            <div class="font-weight-500">Mozelle Belt</div>
                            <small class="text-muted">Web Designer
                            </small>
                        </div>
                        <div class="ml-auto">
                            <a href="#" class="btn btn-sm  btn-light">Follow</a>
                        </div>
                    </div>
                    <div class="list-group-item d-flex  align-items-center">
                        <div class="mr-2">
                            <span class="avatar avatar-md brround cover-image" data-image-src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/images/users/female/6.jpg" style="background: url(&quot;<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/images/users/female/6.jpg&quot;) center center;"></span>
                        </div>
                        <div class="">
                            <div class="font-weight-500">Alina Bernier</div>
                            <small class="text-muted">Administrator
                            </small>
                        </div>
                        <div class="ml-auto">
                            <a href="#" class="btn btn-sm  btn-light">Follow</a>
                        </div>
                    </div>
                    <div class="list-group-item d-flex  align-items-center">
                        <div class="mr-2">
                            <span class="avatar avatar-md brround cover-image" data-image-src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/images/users/male/5.jpg" style="background: url(&quot;<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/images/users/male/5.jpg&quot;) center center;"></span>
                        </div>
                        <div class="">
                            <div class="font-weight-500">Isidro Heide</div>
                            <small class="text-muted">Web Designer
                            </small>
                        </div>
                        <div class="ml-auto">
                            <a href="#" class="btn btn-sm  btn-light">Follow</a>
                        </div>
                    </div>
                    <div class="list-group-item d-flex  align-items-center">
                        <div class="mr-2">
                            <span class="avatar avatar-md brround cover-image" data-image-src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/images/users/male/6.jpg" style="background: url(&quot;<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/images/users/male/6.jpg&quot;) center center;"></span>
                        </div>
                        <div class="">
                            <div class="font-weight-500">Isidro Heide</div>
                            <small class="text-muted">Web Designer
                            </small>
                        </div>
                        <div class="ml-auto">
                            <a href="#" class="btn btn-sm  btn-light">Follow</a>
                        </div>
                    </div>
                    <div class="list-group-item d-flex  align-items-center">
                        <div class="mr-2">
                            <span class="avatar avatar-md brround cover-image" data-image-src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/images/users/male/2.jpg" style="background: url(&quot;<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/images/users/male/2.jpg&quot;) center center;"></span>
                        </div>
                        <div class="">
                            <div class="font-weight-500">Isidro Heide</div>
                            <small class="text-muted">Web Designer
                            </small>
                        </div>
                        <div class="ml-auto">
                            <a href="#" class="btn btn-sm  btn-light">Follow</a>
                        </div>
                    </div>
                    <div class="list-group-item d-flex  align-items-center">
                        <div class="mr-2">
                            <span class="avatar avatar-md brround cover-image" data-image-src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/images/users/male/4.jpg" style="background: url(&quot;<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/images/users/male/2.jpg&quot;) center center;"></span>
                        </div>
                        <div class="">
                            <div class="font-weight-500">Isidro Heide</div>
                            <small class="text-muted">Web Designer
                            </small>
                        </div>
                        <div class="ml-auto">
                            <a href="#" class="btn btn-sm  btn-light">Follow</a>
                        </div>
                    </div>
                    <div class="list-group-item d-flex  align-items-center">
                        <div class="mr-2">
                            <span class="avatar avatar-md brround cover-image" data-image-src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/images/users/male/5.jpg" style="background: url(&quot;<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/images/users/male/2.jpg&quot;) center center;"></span>
                        </div>
                        <div class="">
                            <div class="font-weight-500">Isidro Heide</div>
                            <small class="text-muted">Web Designer
                            </small>
                        </div>
                        <div class="ml-auto">
                            <a href="#" class="btn btn-sm  btn-light">Follow</a>
                        </div>
                    </div>
                    <div class="list-group-item d-flex  align-items-center">
                        <div class="mr-2">
                            <span class="avatar avatar-md brround cover-image" data-image-src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/images/users/male/2.jpg" style="background: url(&quot;<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/images/users/male/2.jpg&quot;) center center;"></span>
                        </div>
                        <div class="">
                            <div class="font-weight-500">Isidro Heide</div>
                            <small class="text-muted">Web Designer
                            </small>
                        </div>
                        <div class="ml-auto">
                            <a href="#" class="btn btn-sm  btn-light">Follow</a>
                        </div>
                    </div>
                    <div class="list-group-item d-flex  align-items-center border-bottom-0">
                        <div class="mr-2">
                            <span class="avatar avatar-md brround cover-image" data-image-src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/images/users/female/3.jpg" style="background: url(&quot;<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/images/users/female/3.jpg&quot;) center center;"></span>
                        </div>
                        <div class="">
                            <div class="font-weight-500">Florinda Carasco</div>
                            <small class="text-muted">Project Manager
                            </small>
                        </div>
                        <div class="ml-auto">
                            <a href="#" class="btn btn-sm  btn-light">Follow</a>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tab3">
                    <div class="">
                        <div class="d-flex p-3">
                            <label class="custom-control custom-checkbox mb-0">
                                <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1" checked="">
                                <span class="custom-control-label">Do Even More..</span>
                            </label>
                            <span class="ml-auto">
										<i class="si si-pencil text-primary mr-2" data-toggle="tooltip" title=""  data-placement="top" data-original-title="Edit"></i>
										<i class="si si-trash text-danger mr-2" data-toggle="tooltip" title="" data-placement="top" data-original-title="Delete"></i>
									</span>
                        </div>
                        <div class="d-flex p-3 border-top">
                            <label class="custom-control custom-checkbox mb-0">
                                <input type="checkbox" class="custom-control-input" name="example-checkbox2" value="option2" checked="">
                                <span class="custom-control-label">Find an idea.</span>
                            </label>
                            <span class="ml-auto">
										<i class="si si-pencil text-primary mr-2" data-toggle="tooltip" title=""  data-placement="top" data-original-title="Edit"></i>
										<i class="si si-trash text-danger mr-2" data-toggle="tooltip" title="" data-placement="top" data-original-title="Delete"></i>
									</span>
                        </div>
                        <div class="d-flex p-3 border-top">
                            <label class="custom-control custom-checkbox mb-0">
                                <input type="checkbox" class="custom-control-input" name="example-checkbox3" value="option3" checked="">
                                <span class="custom-control-label">Hangout with friends</span>
                            </label>
                            <span class="ml-auto">
										<i class="si si-pencil text-primary mr-2" data-toggle="tooltip" title=""  data-placement="top" data-original-title="Edit"></i>
										<i class="si si-trash text-danger mr-2" data-toggle="tooltip" title="" data-placement="top" data-original-title="Delete"></i>
									</span>
                        </div>
                        <div class="d-flex p-3 border-top">
                            <label class="custom-control custom-checkbox mb-0">
                                <input type="checkbox" class="custom-control-input" name="example-checkbox4" value="option4" >
                                <span class="custom-control-label">Do Something else</span>
                            </label>
                            <span class="ml-auto">
										<i class="si si-pencil text-primary mr-2" data-toggle="tooltip" title=""  data-placement="top" data-original-title="Edit"></i>
										<i class="si si-trash text-danger mr-2" data-toggle="tooltip" title="" data-placement="top" data-original-title="Delete"></i>
									</span>
                        </div>
                        <div class="d-flex p-3 border-top">
                            <label class="custom-control custom-checkbox mb-0">
                                <input type="checkbox" class="custom-control-input" name="example-checkbox5" value="option5" >
                                <span class="custom-control-label">Eat healthy, Eat Fresh..</span>
                            </label>
                            <span class="ml-auto">
										<i class="si si-pencil text-primary mr-2" data-toggle="tooltip" title=""  data-placement="top" data-original-title="Edit"></i>
										<i class="si si-trash text-danger mr-2" data-toggle="tooltip" title="" data-placement="top" data-original-title="Delete"></i>
									</span>
                        </div>
                        <div class="d-flex p-3 border-top">
                            <label class="custom-control custom-checkbox mb-0">
                                <input type="checkbox" class="custom-control-input" name="example-checkbox6" value="option6" checked="">
                                <span class="custom-control-label">Finsh something more..</span>
                            </label>
                            <span class="ml-auto">
										<i class="si si-pencil text-primary mr-2" data-toggle="tooltip" title=""  data-placement="top" data-original-title="Edit"></i>
										<i class="si si-trash text-danger mr-2" data-toggle="tooltip" title="" data-placement="top" data-original-title="Delete"></i>
									</span>
                        </div>
                        <div class="d-flex p-3 border-top">
                            <label class="custom-control custom-checkbox mb-0">
                                <input type="checkbox" class="custom-control-input" name="example-checkbox7" value="option7" checked="">
                                <span class="custom-control-label">Do something more</span>
                            </label>
                            <span class="ml-auto">
										<i class="si si-pencil text-primary mr-2" data-toggle="tooltip" title=""  data-placement="top" data-original-title="Edit"></i>
										<i class="si si-trash text-danger mr-2" data-toggle="tooltip" title="" data-placement="top" data-original-title="Delete"></i>
									</span>
                        </div>
                        <div class="d-flex p-3 border-top">
                            <label class="custom-control custom-checkbox mb-0">
                                <input type="checkbox" class="custom-control-input" name="example-checkbox8" value="option8" >
                                <span class="custom-control-label">Updated more files</span>
                            </label>
                            <span class="ml-auto">
										<i class="si si-pencil text-primary mr-2" data-toggle="tooltip" title=""  data-placement="top" data-original-title="Edit"></i>
										<i class="si si-trash text-danger mr-2" data-toggle="tooltip" title="" data-placement="top" data-original-title="Delete"></i>
									</span>
                        </div>
                        <div class="d-flex p-3 border-top">
                            <label class="custom-control custom-checkbox mb-0">
                                <input type="checkbox" class="custom-control-input" name="example-checkbox9" value="option9" >
                                <span class="custom-control-label">System updated</span>
                            </label>
                            <span class="ml-auto">
										<i class="si si-pencil text-primary mr-2" data-toggle="tooltip" title=""  data-placement="top" data-original-title="Edit"></i>
										<i class="si si-trash text-danger mr-2" data-toggle="tooltip" title="" data-placement="top" data-original-title="Delete"></i>
									</span>
                        </div>
                        <div class="d-flex p-3 border-top">
                            <label class="custom-control custom-checkbox mb-0">
                                <input type="checkbox" class="custom-control-input" name="example-checkbox10" value="option10" >
                                <span class="custom-control-label">Settings Changings...</span>
                            </label>
                            <span class="ml-auto">
										<i class="si si-pencil text-primary mr-2" data-toggle="tooltip" title=""  data-placement="top" data-original-title="Edit"></i>
										<i class="si si-trash text-danger mr-2" data-toggle="tooltip" title="" data-placement="top" data-original-title="Delete"></i>
									</span>
                        </div><div class="d-flex p-3 border-top">
                            <label class="custom-control custom-checkbox mb-0">
                                <input type="checkbox" class="custom-control-input" name="example-checkbox9" value="option9" >
                                <span class="custom-control-label">System updated</span>
                            </label>
                            <span class="ml-auto">
										<i class="si si-pencil text-primary mr-2" data-toggle="tooltip" title=""  data-placement="top" data-original-title="Edit"></i>
										<i class="si si-trash text-danger mr-2" data-toggle="tooltip" title="" data-placement="top" data-original-title="Delete"></i>
									</span>
                        </div>
                        <div class="d-flex p-3 border-top border-bottom">
                            <label class="custom-control custom-checkbox mb-0">
                                <input type="checkbox" class="custom-control-input" name="example-checkbox10" value="option10" >
                                <span class="custom-control-label">Settings Changings...</span>
                            </label>
                            <span class="ml-auto">
										<i class="si si-pencil text-primary mr-2" data-toggle="tooltip" title=""  data-placement="top" data-original-title="Edit"></i>
										<i class="si si-trash text-danger mr-2" data-toggle="tooltip" title="" data-placement="top" data-original-title="Delete"></i>
									</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- End Rightsidebar-->

    <!-- FOOTER -->
    <footer class="footer left-footer">
        <div class="container">
            <div class="row align-items-center flex-row-reverse">
                <div class="col-md-12 col-sm-12 text-center">
                    Copyright © 2019 <a href="#">Lisa</a>. Designed by <a href="https://aiken.mx/">Aiken Sistemas</a> All rights reserved.
                </div>
            </div>
        </div>
    </footer>
    <!-- FOOTER END -->
</div>

<!-- BACK-TO-TOP -->
<a href="#top" id="back-to-top"><i class="fa fa-angle-double-up"></i></a>



<!-- BOOTSTRAP SCRIPTS JS-->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/js/vendors/bootstrap.bundle.min.js"></script>

<!-- SPARKLINE JS -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/js/vendors/jquery.sparkline.min.js"></script>

<!-- CHART-CIRCLE JS-->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/js/vendors/circle-progress.min.js"></script>

<!-- RATING STAR JS-->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/plugins/rating/rating-stars.js"></script>

<!-- INPUT MASK JS-->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/plugins/input-mask/input-mask.min.js"></script>

<!-- C3.JS CHART JS -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/plugins/charts-c3/d3.v5.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/plugins/charts-c3/c3-chart.js"></script>

<!-- POPOVER JS -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/js/popover.js"></script>

<!-- SWEET-ALERT JS -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/plugins/sweet-alert/sweetalert.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/js/sweet-alert.js"></script>

<!-- CHARTJS CHART JS-->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/plugins/chart/chart.bundle.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/plugins/chart/utils.js"></script>

<!-- PIETY CHART JS-->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/plugins/peitychart/jquery.peity.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/plugins/peitychart/peitychart.init.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/plugins/peitychart/peitychart.js"></script>

<!--SIDEMENU JS-->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/plugins/sidemenu/sidemenu.js"></script>

<!-- SIDEMENU-RESPONSIVE-TABS JS-->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/plugins/sidemenu-responsive-tabs/js/sidemenu-responsive-tabs.js"></script>

<!--LEFT-MENU JS-->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/js/left-menu.js"></script>

<!-- P-SCROLL JS -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/plugins/p-scroll/p-scroll.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/plugins/p-scroll/p-scroll-leftmenu.js"></script>

<!-- COUNTERS JS-->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/plugins/counters/counterup.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/plugins/counters/waypoints.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/plugins/counters/counters-1.js"></script>

<!-- SIDEBAR JS -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/plugins/right-sidebar/right-sidebar.js"></script>

<!-- INDEX-SCRIPTS  JS-->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/js/index.js"></script>


<!-- DATA TABLE JS-->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/plugins/Datatable/js/jquery.dataTables.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/plugins/Datatable/js/dataTables.bootstrap4.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/plugins/Datatable/js/dataTables.buttons.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/plugins/Datatable/js/buttons.bootstrap4.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/plugins/Datatable/js/jszip.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/plugins/Datatable/js/pdfmake.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/plugins/Datatable/js/vfs_fonts.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/plugins/Datatable/js/buttons.html5.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/plugins/Datatable/js/buttons.print.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/plugins/Datatable/js/buttons.colVis.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/plugins/datatable/dataTables.responsive.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/plugins/datatable/responsive.bootstrap4.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/plugins/datatable/datatable.js"></script>

<!-- CHARTJS JS -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/plugins/chart/chart.bundle.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/plugins/chart/utils.js"></script>

<!-- C3-PIE CHART JS -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/js/chart.js"></script>

<!-- CUSTOM JS -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/js/custom.js"></script>

<!-- FILE UPLOADES JS -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/plugins/fileupload/js/fileupload.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/plugins/fileupload/js/file-upload.js"></script>



<!-- JQUERY SCRIPTS JS -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/assets/js/vendors/jquery-3.2.1.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/js/vendors/jquery.validate.js"></script>

</body>
</html>