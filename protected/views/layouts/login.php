<!DOCTYPE html>
<html lang="es">

<head>
    <title>LISA | LOGISTIC INTERNAL SOFTWARE ADMINISTRATION</title>
    <!-- HTML5 Shim and Respond.js IE10 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 10]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="#">
    <meta name="keywords" content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
    <meta name="author" content="#">
    <!-- Favicon icon -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/css/style.css" rel="stylesheet"/>
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/css/color-style.css" rel="stylesheet"/>
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/css/skins-modes.css" rel="stylesheet"/>

    <!-- SINGLE-PAGE CSS -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/plugins/single-page/css/single-page.css" rel="stylesheet" type="text/css">

    <!--- FONT-ICONS CSS -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/css/icons.css" rel="stylesheet"/>
    <!-- JQUERY SCRIPTS JS-->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/js/vendors/jquery-3.2.1.min.js"></script>


</head>

<body class="fix-menu app">
    <!-- Pre-loader start -->
<!--    <div class="theme-loader">
    <div class="ball-scale">
        <div class='contain'>
            <div class="ring"><div class="frame"></div></div>
            <div class="ring"><div class="frame"></div></div>
            <div class="ring"><div class="frame"></div></div>
            <div class="ring"><div class="frame"></div></div>
            <div class="ring"><div class="frame"></div></div>
            <div class="ring"><div class="frame"></div></div>
            <div class="ring"><div class="frame"></div></div>
            <div class="ring"><div class="frame"></div></div>
            <div class="ring"><div class="frame"></div></div>
            <div class="ring"><div class="frame"></div></div>
        </div>
    </div>-->
</div>
    <!-- Pre-loader end -->
    <div class="login-img" style=" background-image: url('<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/images/bg/bg01.jpg'); overflow: hidden; overflow-y: hidden" >

        <!-- GLOABAL LOADER -->
        <div id="global-loader">
            <img src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/images/svgs/loader.svg" class="loader-img" alt="Loader">
        </div>


                <!-- CONTAINER OPEN -->

                <div class="row h-100" >
                    <div class="col-lg-4 col-md-3 col-sm-12 bg-white" style="">

                            <div class="p-8" style="margin-top: 30%">
                                <?php echo $content; ?>
                            </div>

                    </div>

                </div>


    </div>
    <!-- Warning Section Starts -->
    <!-- Older IE warning message -->
    <!--[if lt IE 10]>

<![endif]-->


    <!-- BOOTSTRAP SCRIPTS JS-->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/js/vendors/bootstrap.bundle.min.js"></script>

    <!-- SPARKLINE JS-->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/js/vendors/jquery.sparkline.min.js"></script>

    <!-- CHART-CIRCLE JS-->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/js/vendors/circle-progress.min.js"></script>

    <!-- RATING STAR JS-->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/plugins/rating/rating-stars.js"></script>

    <!-- INPUT MASK JS-->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/plugins/input-mask/input-mask.min.js"></script>

    <!-- CUSTOM JS-->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/styles/assets/js/custom.js"></script>

</body>

</html>
