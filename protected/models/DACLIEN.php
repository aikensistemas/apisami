<?php

/**
 * This is the model class for table "DACLIEN".
 *
 * The followings are the available columns in table 'DACLIEN':
 * @property string $DNUMCTE
 * @property string $DRAZON
 * @property string $DCALLE
 * @property string $DNUMEXT
 * @property string $DNUMINT
 * @property string $DREFEREN
 * @property string $DCOLON
 * @property string $DDELMUN
 * @property string $DCIUDAD
 * @property string $DCODEST
 * @property string $DPAIS
 * @property string $DCODPOS
 * @property string $DLADA
 * @property string $DTELEF
 * @property string $DTELE2
 * @property string $DTELE3
 * @property string $DFAX
 * @property string $DFAX2
 * @property string $DRFC
 * @property string $DCURP
 * @property string $DGLN
 * @property string $DNUMPRV
 * @property string $DATENCI
 * @property string $DEMAIL1
 * @property string $DATENC2
 * @property string $DEMAIL2
 * @property string $DCOMENT
 * @property string $DCOMEN2
 * @property string $DOBSERV
 * @property string $DTIPO
 * @property string $DDIVI
 * @property string $DZONA
 * @property string $DCONPAG
 * @property string $DAGENTE
 * @property string $DLISPRE
 * @property string $DPREESP
 * @property string $DDESCTE
 * @property string $DLIMITE
 * @property string $DMATRIZ
 * @property string $DDIAREV
 * @property boolean $DREVIPM
 * @property string $DHORARE
 * @property string $DDIAPAG
 * @property boolean $DPAGOPM
 * @property string $DHORAPA
 * @property string $DFECHAU
 * @property string $DPASSCTE
 * @property boolean $DBAJA
 * @property boolean $DCORREO
 * @property boolean $DLIGA
 * @property string $DEMAILFE
 * @property string $DNUMCOP
 * @property string $DNUMADDEN
 * @property string $DTIPOPAGO
 * @property string $DNUMCTAPAG
 * @property string $DCTAVTA
 * @property string $DNUMCENVTA
 * @property string $DCTACTO
 * @property string $DNUMCENCTO
 * @property string $DTAXID
 * @property string $DUCFDI
 * @property string $DEXPCONF
 * @property string $DEMAILPAGO
 * @property string $DADDEPAGO
 */
class DACLIEN extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'DACLIEN';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('DNUMCTE, DRAZON, DCALLE, DNUMEXT, DNUMINT, DREFEREN, DCOLON, DDELMUN, DCIUDAD, DCODEST, DPAIS, DCODPOS, DLADA, DTELEF, DTELE2, DTELE3, DFAX, DFAX2, DRFC, DCURP, DGLN, DNUMPRV, DATENCI, DEMAIL1, DATENC2, DEMAIL2, DCOMENT, DCOMEN2, DOBSERV, DTIPO, DDIVI, DZONA, DCONPAG, DAGENTE, DLISPRE, DPREESP, DDESCTE, DLIMITE, DDIAREV, DREVIPM, DHORARE, DDIAPAG, DPAGOPM, DHORAPA, DFECHAU, DBAJA, DCORREO, DLIGA, DEMAILFE, DNUMCOP, DNUMADDEN, DNUMCTAPAG, DEMAILPAGO, DADDEPAGO', 'required'),
			array('DNUMCTE, DMATRIZ, DNUMCENVTA, DNUMCENCTO', 'length', 'max'=>6),
			array('DRAZON, DEMAIL1, DEMAIL2, DEMAILFE, DEMAILPAGO', 'length', 'max'=>200),
			array('DCALLE, DREFEREN, DATENCI, DATENC2, DCOMENT, DCOMEN2, DPASSCTE, DEXPCONF', 'length', 'max'=>50),
			array('DNUMEXT, DNUMINT, DCODPOS, DNUMPRV', 'length', 'max'=>10),
			array('DCOLON, DDELMUN, DCIUDAD, DTAXID', 'length', 'max'=>30),
			array('DCODEST, DTIPO, DDIVI, DZONA, DCONPAG, DAGENTE, DTIPOPAGO, DUCFDI', 'length', 'max'=>3),
			array('DPAIS', 'length', 'max'=>24),
			array('DLADA, DDESCTE, DNUMCOP', 'length', 'max'=>5),
			array('DTELEF, DTELE2, DTELE3, DFAX, DFAX2, DLIMITE', 'length', 'max'=>15),
			array('DRFC', 'length', 'max'=>13),
			array('DCURP', 'length', 'max'=>18),
			array('DGLN', 'length', 'max'=>16),
			array('DLISPRE, DPREESP', 'length', 'max'=>4),
			array('DDIAREV, DDIAPAG', 'length', 'max'=>1),
			array('DHORARE, DHORAPA, DNUMCTAPAG, DCTAVTA, DCTACTO', 'length', 'max'=>20),
			array('DNUMADDEN, DADDEPAGO', 'length', 'max'=>2),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('DNUMCTE, DRAZON, DCALLE, DNUMEXT, DNUMINT, DREFEREN, DCOLON, DDELMUN, DCIUDAD, DCODEST, DPAIS, DCODPOS, DLADA, DTELEF, DTELE2, DTELE3, DFAX, DFAX2, DRFC, DCURP, DGLN, DNUMPRV, DATENCI, DEMAIL1, DATENC2, DEMAIL2, DCOMENT, DCOMEN2, DOBSERV, DTIPO, DDIVI, DZONA, DCONPAG, DAGENTE, DLISPRE, DPREESP, DDESCTE, DLIMITE, DMATRIZ, DDIAREV, DREVIPM, DHORARE, DDIAPAG, DPAGOPM, DHORAPA, DFECHAU, DPASSCTE, DBAJA, DCORREO, DLIGA, DEMAILFE, DNUMCOP, DNUMADDEN, DTIPOPAGO, DNUMCTAPAG, DCTAVTA, DNUMCENVTA, DCTACTO, DNUMCENCTO, DTAXID, DUCFDI, DEXPCONF, DEMAILPAGO, DADDEPAGO', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'DNUMCTE' => 'Dnumcte',
			'DRAZON' => 'Drazon',
			'DCALLE' => 'Dcalle',
			'DNUMEXT' => 'Dnumext',
			'DNUMINT' => 'Dnumint',
			'DREFEREN' => 'Dreferen',
			'DCOLON' => 'Dcolon',
			'DDELMUN' => 'Ddelmun',
			'DCIUDAD' => 'Dciudad',
			'DCODEST' => 'Dcodest',
			'DPAIS' => 'Dpais',
			'DCODPOS' => 'Dcodpos',
			'DLADA' => 'Dlada',
			'DTELEF' => 'Dtelef',
			'DTELE2' => 'Dtele2',
			'DTELE3' => 'Dtele3',
			'DFAX' => 'Dfax',
			'DFAX2' => 'Dfax2',
			'DRFC' => 'Drfc',
			'DCURP' => 'Dcurp',
			'DGLN' => 'Dgln',
			'DNUMPRV' => 'Dnumprv',
			'DATENCI' => 'Datenci',
			'DEMAIL1' => 'Demail1',
			'DATENC2' => 'Datenc2',
			'DEMAIL2' => 'Demail2',
			'DCOMENT' => 'Dcoment',
			'DCOMEN2' => 'Dcomen2',
			'DOBSERV' => 'Dobserv',
			'DTIPO' => 'Dtipo',
			'DDIVI' => 'Ddivi',
			'DZONA' => 'Dzona',
			'DCONPAG' => 'Dconpag',
			'DAGENTE' => 'Dagente',
			'DLISPRE' => 'Dlispre',
			'DPREESP' => 'Dpreesp',
			'DDESCTE' => 'Ddescte',
			'DLIMITE' => 'Dlimite',
			'DMATRIZ' => 'Dmatriz',
			'DDIAREV' => 'Ddiarev',
			'DREVIPM' => 'Drevipm',
			'DHORARE' => 'Dhorare',
			'DDIAPAG' => 'Ddiapag',
			'DPAGOPM' => 'Dpagopm',
			'DHORAPA' => 'Dhorapa',
			'DFECHAU' => 'Dfechau',
			'DPASSCTE' => 'Dpasscte',
			'DBAJA' => 'Dbaja',
			'DCORREO' => 'Dcorreo',
			'DLIGA' => 'Dliga',
			'DEMAILFE' => 'Demailfe',
			'DNUMCOP' => 'Dnumcop',
			'DNUMADDEN' => 'Dnumadden',
			'DTIPOPAGO' => 'Dtipopago',
			'DNUMCTAPAG' => 'Dnumctapag',
			'DCTAVTA' => 'Dctavta',
			'DNUMCENVTA' => 'Dnumcenvta',
			'DCTACTO' => 'Dctacto',
			'DNUMCENCTO' => 'Dnumcencto',
			'DTAXID' => 'Dtaxid',
			'DUCFDI' => 'Ducfdi',
			'DEXPCONF' => 'Dexpconf',
			'DEMAILPAGO' => 'Demailpago',
			'DADDEPAGO' => 'Daddepago',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('DNUMCTE',$this->DNUMCTE,true);
		$criteria->compare('DRAZON',$this->DRAZON,true);
		$criteria->compare('DCALLE',$this->DCALLE,true);
		$criteria->compare('DNUMEXT',$this->DNUMEXT,true);
		$criteria->compare('DNUMINT',$this->DNUMINT,true);
		$criteria->compare('DREFEREN',$this->DREFEREN,true);
		$criteria->compare('DCOLON',$this->DCOLON,true);
		$criteria->compare('DDELMUN',$this->DDELMUN,true);
		$criteria->compare('DCIUDAD',$this->DCIUDAD,true);
		$criteria->compare('DCODEST',$this->DCODEST,true);
		$criteria->compare('DPAIS',$this->DPAIS,true);
		$criteria->compare('DCODPOS',$this->DCODPOS,true);
		$criteria->compare('DLADA',$this->DLADA,true);
		$criteria->compare('DTELEF',$this->DTELEF,true);
		$criteria->compare('DTELE2',$this->DTELE2,true);
		$criteria->compare('DTELE3',$this->DTELE3,true);
		$criteria->compare('DFAX',$this->DFAX,true);
		$criteria->compare('DFAX2',$this->DFAX2,true);
		$criteria->compare('DRFC',$this->DRFC,true);
		$criteria->compare('DCURP',$this->DCURP,true);
		$criteria->compare('DGLN',$this->DGLN,true);
		$criteria->compare('DNUMPRV',$this->DNUMPRV,true);
		$criteria->compare('DATENCI',$this->DATENCI,true);
		$criteria->compare('DEMAIL1',$this->DEMAIL1,true);
		$criteria->compare('DATENC2',$this->DATENC2,true);
		$criteria->compare('DEMAIL2',$this->DEMAIL2,true);
		$criteria->compare('DCOMENT',$this->DCOMENT,true);
		$criteria->compare('DCOMEN2',$this->DCOMEN2,true);
		$criteria->compare('DOBSERV',$this->DOBSERV,true);
		$criteria->compare('DTIPO',$this->DTIPO,true);
		$criteria->compare('DDIVI',$this->DDIVI,true);
		$criteria->compare('DZONA',$this->DZONA,true);
		$criteria->compare('DCONPAG',$this->DCONPAG,true);
		$criteria->compare('DAGENTE',$this->DAGENTE,true);
		$criteria->compare('DLISPRE',$this->DLISPRE,true);
		$criteria->compare('DPREESP',$this->DPREESP,true);
		$criteria->compare('DDESCTE',$this->DDESCTE,true);
		$criteria->compare('DLIMITE',$this->DLIMITE,true);
		$criteria->compare('DMATRIZ',$this->DMATRIZ,true);
		$criteria->compare('DDIAREV',$this->DDIAREV,true);
		$criteria->compare('DREVIPM',$this->DREVIPM);
		$criteria->compare('DHORARE',$this->DHORARE,true);
		$criteria->compare('DDIAPAG',$this->DDIAPAG,true);
		$criteria->compare('DPAGOPM',$this->DPAGOPM);
		$criteria->compare('DHORAPA',$this->DHORAPA,true);
		$criteria->compare('DFECHAU',$this->DFECHAU,true);
		$criteria->compare('DPASSCTE',$this->DPASSCTE,true);
		$criteria->compare('DBAJA',$this->DBAJA);
		$criteria->compare('DCORREO',$this->DCORREO);
		$criteria->compare('DLIGA',$this->DLIGA);
		$criteria->compare('DEMAILFE',$this->DEMAILFE,true);
		$criteria->compare('DNUMCOP',$this->DNUMCOP,true);
		$criteria->compare('DNUMADDEN',$this->DNUMADDEN,true);
		$criteria->compare('DTIPOPAGO',$this->DTIPOPAGO,true);
		$criteria->compare('DNUMCTAPAG',$this->DNUMCTAPAG,true);
		$criteria->compare('DCTAVTA',$this->DCTAVTA,true);
		$criteria->compare('DNUMCENVTA',$this->DNUMCENVTA,true);
		$criteria->compare('DCTACTO',$this->DCTACTO,true);
		$criteria->compare('DNUMCENCTO',$this->DNUMCENCTO,true);
		$criteria->compare('DTAXID',$this->DTAXID,true);
		$criteria->compare('DUCFDI',$this->DUCFDI,true);
		$criteria->compare('DEXPCONF',$this->DEXPCONF,true);
		$criteria->compare('DEMAILPAGO',$this->DEMAILPAGO,true);
		$criteria->compare('DADDEPAGO',$this->DADDEPAGO,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DACLIEN the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
