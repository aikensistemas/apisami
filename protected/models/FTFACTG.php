<?php

/**
 * This is the model class for table "FTFACTG".
 *
 * The followings are the available columns in table 'FTFACTG':
 * @property string $MCVEMOV
 * @property string $MFOLIOG
 * @property string $MNUMCTE
 * @property string $MNUMCON
 * @property string $MALMACE
 * @property string $MMATRIZ
 * @property string $MAGENTE
 * @property string $MFECHAM
 * @property string $MFECHAR
 * @property string $MHORAM
 * @property string $MFECHAV
 * @property string $MNUMAPRO
 * @property string $MDESCTE
 * @property string $MDESCT2
 * @property string $MDESCT3
 * @property string $MCONPAG
 * @property string $MCVEIVA
 * @property string $MCVEIVAR
 * @property string $MCVEIEPS
 * @property string $MCVEISRR
 * @property string $MRAZON
 * @property string $MCALLE
 * @property string $MNUMEXT
 * @property string $MNUMINT
 * @property string $MREFEREN
 * @property string $MCOLON
 * @property string $MDELMUN
 * @property string $MCIUDAD
 * @property string $MCODEST
 * @property string $MCODPOS
 * @property string $MGLN
 * @property string $MOBSERV
 * @property string $MCVEMON
 * @property string $MTIPOCA
 * @property string $MPEDCTE
 * @property string $MLAB
 * @property string $MFLETE
 * @property string $MEMBARQ
 * @property string $MTALON
 * @property string $MPESON
 * @property string $MPESOB
 * @property string $MIMPFLE
 * @property string $MIMPOTR
 * @property string $MLISPRE
 * @property string $MPREESP
 * @property boolean $MIVAINC
 * @property boolean $MIEPSINC
 * @property boolean $MIVAREN
 * @property boolean $MIVARENR
 * @property boolean $MIEPSREN
 * @property boolean $MVIGIM
 * @property string $MIMPBRU
 * @property string $MIMPDES
 * @property string $MDESREN
 * @property string $MIMPCOS
 * @property string $MIMPIVA
 * @property string $MIMPIVAR
 * @property string $MIMPISRR
 * @property string $MIMPIEPS
 * @property string $MCOMAGE
 * @property string $MINICIA
 * @property string $MINIAUT
 * @property string $MCVECOT
 * @property string $MNUMCOT
 * @property string $MYEARAPRO
 * @property string $MNUMCER
 * @property string $MSELLO
 * @property string $MCADORIG
 * @property string $MUUID
 * @property string $MFECTIMBRE
 * @property string $MNUMCERSAT
 * @property string $MSELLOSAT
 * @property string $MXML
 * @property boolean $MENVIADO
 * @property string $MTIPOPAGO
 * @property string $MNUMCTAPAG
 * @property boolean $MPARCIAL
 * @property boolean $MBAJA
 * @property string $MCVEFAC
 * @property string $MNUMFAC
 * @property string $MTIPOPE
 * @property string $MCVEPEDIM
 * @property boolean $MCERORI
 * @property boolean $MSUBDIV
 * @property string $MNUMCERORI
 * @property string $MCVEINC
 * @property string $MNEXPCONFI
 * @property string $MTIPOCAEXP
 * @property string $MTOTUSD
 * @property string $MOBSEXP
 * @property string $MCONFSAT
 * @property string $MMETPAG
 * @property string $MUCFDI
 * @property string $MCVEREL
 * @property string $MFOLREL
 * @property string $MTIPOREL
 * @property string $MUUIDREL
 * @property string $MPORLOC
 * @property boolean $MLOCINC
 * @property boolean $MLOCREN
 * @property string $MPORLOCR
 * @property string $MIMPIEPSC
 * @property string $MIMPLOC
 * @property string $MIMPLOCR
 * @property string $MDESCBD
 * @property string $MCVEPSBD
 * @property string $MMEDSATBD
 * @property boolean $MVTAGLO
 * @property boolean $MNOREP
 */
class FTFACTG extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'FTFACTG';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('MCVEMOV, MFOLIOG, MNUMCTE, MALMACE, MAGENTE, MFECHAM, MFECHAV, MDESCTE, MDESCT2, MDESCT3, MCONPAG, MCVEIVA, MCVEIVAR, MCVEIEPS, MRAZON, MCALLE, MNUMEXT,  MCOLON,  MCIUDAD, MCODPOS,  MCVEMON, MTIPOCA, MPESON, MPESOB, MIMPFLE, MIMPOTR, MLISPRE, MPREESP, MIVAINC, MIEPSINC, MIVAREN, MIVARENR, MIEPSREN, MVIGIM, MIMPBRU, MIMPDES, MDESREN, MIMPCOS, MIMPIVA, MIMPIVAR, MIMPISRR, MIMPIEPS, MCOMAGE, MENVIADO, MPARCIAL, MBAJA,  MCERORI,  MPORLOC, MLOCINC, MLOCREN, MPORLOCR, MIMPIEPSC, MIMPLOC, MIMPLOCR, MVTAGLO, MNOREP', 'required'),
			array('MCVEMOV, MFOLIOG, MNUMCTE, MNUMCON, MMATRIZ, MFLETE, MTALON, MCVECOT, MNUMCOT, MCVEFAC, MNUMFAC, MCVEREL, MFOLREL', 'length', 'max'=>6),
			array('MALMACE, MAGENTE, MCONPAG, MCVEIVA, MCVEISRR, MCODEST, MCVEMON, MTIPOPAGO, MCVEINC, MMETPAG, MUCFDI, MMEDSATBD', 'length', 'max'=>3),
			array('MHORAM, MINICIA, MINIAUT', 'length', 'max'=>8),
			array('MNUMAPRO, MNUMEXT, MNUMINT, MCODPOS, MCVEPSBD', 'length', 'max'=>10),
			array('MDESCTE, MDESCT2, MDESCT3, MCVEIEPS, MCONFSAT, MPORLOC, MPORLOCR', 'length', 'max'=>5),
			array('MCVEIVAR', 'length', 'max'=>13),
			array('MRAZON', 'length', 'max'=>200),
			array('MCALLE, MREFEREN, MNEXPCONFI', 'length', 'max'=>50),
			array('MCOLON, MDELMUN, MCIUDAD', 'length', 'max'=>30),
			array('MGLN', 'length', 'max'=>16),
			array('MOBSERV, MLAB, MIMPFLE, MIMPOTR, MIMPBRU, MIMPDES, MDESREN, MIMPCOS, MIMPIVA, MIMPIVAR, MIMPISRR, MIMPIEPS, MCOMAGE, MIMPIEPSC, MIMPLOC, MIMPLOCR', 'length', 'max'=>15),
			array('MTIPOCA, MPESON, MPESOB', 'length', 'max'=>7),
			array('MPEDCTE, MNUMCER, MFECTIMBRE, MNUMCERSAT, MNUMCTAPAG', 'length', 'max'=>20),
			array('MEMBARQ', 'length', 'max'=>24),
			array('MLISPRE, MPREESP, MYEARAPRO', 'length', 'max'=>4),
			array('MSELLO, MSELLOSAT', 'length', 'max'=>350),
			array('MUUID, MNUMCERORI, MUUIDREL, MDESCBD', 'length', 'max'=>40),
			array('MTIPOPE, MCVEPEDIM', 'length', 'max'=>1),
			array('MTIPOCAEXP', 'length', 'max'=>9),
			array('MTOTUSD', 'length', 'max'=>14),
			array('MTIPOREL', 'length', 'max'=>2),
			array('MFECHAR, MCADORIG, MXML, MSUBDIV', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('MCVEMOV, MFOLIOG, MNUMCTE, MNUMCON, MALMACE, MMATRIZ, MAGENTE, MFECHAM, MFECHAR, MHORAM, MFECHAV, MNUMAPRO, MDESCTE, MDESCT2, MDESCT3, MCONPAG, MCVEIVA, MCVEIVAR, MCVEIEPS, MCVEISRR, MRAZON, MCALLE, MNUMEXT, MNUMINT, MREFEREN, MCOLON, MDELMUN, MCIUDAD, MCODEST, MCODPOS, MGLN, MOBSERV, MCVEMON, MTIPOCA, MPEDCTE, MLAB, MFLETE, MEMBARQ, MTALON, MPESON, MPESOB, MIMPFLE, MIMPOTR, MLISPRE, MPREESP, MIVAINC, MIEPSINC, MIVAREN, MIVARENR, MIEPSREN, MVIGIM, MIMPBRU, MIMPDES, MDESREN, MIMPCOS, MIMPIVA, MIMPIVAR, MIMPISRR, MIMPIEPS, MCOMAGE, MINICIA, MINIAUT, MCVECOT, MNUMCOT, MYEARAPRO, MNUMCER, MSELLO, MCADORIG, MUUID, MFECTIMBRE, MNUMCERSAT, MSELLOSAT, MXML, MENVIADO, MTIPOPAGO, MNUMCTAPAG, MPARCIAL, MBAJA, MCVEFAC, MNUMFAC, MTIPOPE, MCVEPEDIM, MCERORI, MSUBDIV, MNUMCERORI, MCVEINC, MNEXPCONFI, MTIPOCAEXP, MTOTUSD, MOBSEXP, MCONFSAT, MMETPAG, MUCFDI, MCVEREL, MFOLREL, MTIPOREL, MUUIDREL, MPORLOC, MLOCINC, MLOCREN, MPORLOCR, MIMPIEPSC, MIMPLOC, MIMPLOCR, MDESCBD, MCVEPSBD, MMEDSATBD, MVTAGLO, MNOREP', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'MCVEMOV' => 'Mcvemov',
			'MFOLIOG' => 'Mfoliog',
			'MNUMCTE' => 'Mnumcte',
			'MNUMCON' => 'Mnumcon',
			'MALMACE' => 'Malmace',
			'MMATRIZ' => 'Mmatriz',
			'MAGENTE' => 'Magente',
			'MFECHAM' => 'Mfecham',
			'MFECHAR' => 'Mfechar',
			'MHORAM' => 'Mhoram',
			'MFECHAV' => 'Mfechav',
			'MNUMAPRO' => 'Mnumapro',
			'MDESCTE' => 'Mdescte',
			'MDESCT2' => 'Mdesct2',
			'MDESCT3' => 'Mdesct3',
			'MCONPAG' => 'Mconpag',
			'MCVEIVA' => 'Mcveiva',
			'MCVEIVAR' => 'Mcveivar',
			'MCVEIEPS' => 'Mcveieps',
			'MCVEISRR' => 'Mcveisrr',
			'MRAZON' => 'Mrazon',
			'MCALLE' => 'Mcalle',
			'MNUMEXT' => 'Mnumext',
			'MNUMINT' => 'Mnumint',
			'MREFEREN' => 'Mreferen',
			'MCOLON' => 'Mcolon',
			'MDELMUN' => 'Mdelmun',
			'MCIUDAD' => 'Mciudad',
			'MCODEST' => 'Mcodest',
			'MCODPOS' => 'Mcodpos',
			'MGLN' => 'Mgln',
			'MOBSERV' => 'Mobserv',
			'MCVEMON' => 'Mcvemon',
			'MTIPOCA' => 'Mtipoca',
			'MPEDCTE' => 'Mpedcte',
			'MLAB' => 'Mlab',
			'MFLETE' => 'Mflete',
			'MEMBARQ' => 'Membarq',
			'MTALON' => 'Mtalon',
			'MPESON' => 'Mpeson',
			'MPESOB' => 'Mpesob',
			'MIMPFLE' => 'Mimpfle',
			'MIMPOTR' => 'Mimpotr',
			'MLISPRE' => 'Mlispre',
			'MPREESP' => 'Mpreesp',
			'MIVAINC' => 'Mivainc',
			'MIEPSINC' => 'Miepsinc',
			'MIVAREN' => 'Mivaren',
			'MIVARENR' => 'Mivarenr',
			'MIEPSREN' => 'Miepsren',
			'MVIGIM' => 'Mvigim',
			'MIMPBRU' => 'Mimpbru',
			'MIMPDES' => 'Mimpdes',
			'MDESREN' => 'Mdesren',
			'MIMPCOS' => 'Mimpcos',
			'MIMPIVA' => 'Mimpiva',
			'MIMPIVAR' => 'Mimpivar',
			'MIMPISRR' => 'Mimpisrr',
			'MIMPIEPS' => 'Mimpieps',
			'MCOMAGE' => 'Mcomage',
			'MINICIA' => 'Minicia',
			'MINIAUT' => 'Miniaut',
			'MCVECOT' => 'Mcvecot',
			'MNUMCOT' => 'Mnumcot',
			'MYEARAPRO' => 'Myearapro',
			'MNUMCER' => 'Mnumcer',
			'MSELLO' => 'Msello',
			'MCADORIG' => 'Mcadorig',
			'MUUID' => 'Muuid',
			'MFECTIMBRE' => 'Mfectimbre',
			'MNUMCERSAT' => 'Mnumcersat',
			'MSELLOSAT' => 'Msellosat',
			'MXML' => 'Mxml',
			'MENVIADO' => 'Menviado',
			'MTIPOPAGO' => 'Mtipopago',
			'MNUMCTAPAG' => 'Mnumctapag',
			'MPARCIAL' => 'Mparcial',
			'MBAJA' => 'Mbaja',
			'MCVEFAC' => 'Mcvefac',
			'MNUMFAC' => 'Mnumfac',
			'MTIPOPE' => 'Mtipope',
			'MCVEPEDIM' => 'Mcvepedim',
			'MCERORI' => 'Mcerori',
			'MSUBDIV' => 'Msubdiv',
			'MNUMCERORI' => 'Mnumcerori',
			'MCVEINC' => 'Mcveinc',
			'MNEXPCONFI' => 'Mnexpconfi',
			'MTIPOCAEXP' => 'Mtipocaexp',
			'MTOTUSD' => 'Mtotusd',
			'MOBSEXP' => 'Mobsexp',
			'MCONFSAT' => 'Mconfsat',
			'MMETPAG' => 'Mmetpag',
			'MUCFDI' => 'Mucfdi',
			'MCVEREL' => 'Mcverel',
			'MFOLREL' => 'Mfolrel',
			'MTIPOREL' => 'Mtiporel',
			'MUUIDREL' => 'Muuidrel',
			'MPORLOC' => 'Mporloc',
			'MLOCINC' => 'Mlocinc',
			'MLOCREN' => 'Mlocren',
			'MPORLOCR' => 'Mporlocr',
			'MIMPIEPSC' => 'Mimpiepsc',
			'MIMPLOC' => 'Mimploc',
			'MIMPLOCR' => 'Mimplocr',
			'MDESCBD' => 'Mdescbd',
			'MCVEPSBD' => 'Mcvepsbd',
			'MMEDSATBD' => 'Mmedsatbd',
			'MVTAGLO' => 'Mvtaglo',
			'MNOREP' => 'Mnorep',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('MCVEMOV',$this->MCVEMOV,true);
		$criteria->compare('MFOLIOG',$this->MFOLIOG,true);
		$criteria->compare('MNUMCTE',$this->MNUMCTE,true);
		$criteria->compare('MNUMCON',$this->MNUMCON,true);
		$criteria->compare('MALMACE',$this->MALMACE,true);
		$criteria->compare('MMATRIZ',$this->MMATRIZ,true);
		$criteria->compare('MAGENTE',$this->MAGENTE,true);
		$criteria->compare('MFECHAM',$this->MFECHAM,true);
		$criteria->compare('MFECHAR',$this->MFECHAR,true);
		$criteria->compare('MHORAM',$this->MHORAM,true);
		$criteria->compare('MFECHAV',$this->MFECHAV,true);
		$criteria->compare('MNUMAPRO',$this->MNUMAPRO,true);
		$criteria->compare('MDESCTE',$this->MDESCTE,true);
		$criteria->compare('MDESCT2',$this->MDESCT2,true);
		$criteria->compare('MDESCT3',$this->MDESCT3,true);
		$criteria->compare('MCONPAG',$this->MCONPAG,true);
		$criteria->compare('MCVEIVA',$this->MCVEIVA,true);
		$criteria->compare('MCVEIVAR',$this->MCVEIVAR,true);
		$criteria->compare('MCVEIEPS',$this->MCVEIEPS,true);
		$criteria->compare('MCVEISRR',$this->MCVEISRR,true);
		$criteria->compare('MRAZON',$this->MRAZON,true);
		$criteria->compare('MCALLE',$this->MCALLE,true);
		$criteria->compare('MNUMEXT',$this->MNUMEXT,true);
		$criteria->compare('MNUMINT',$this->MNUMINT,true);
		$criteria->compare('MREFEREN',$this->MREFEREN,true);
		$criteria->compare('MCOLON',$this->MCOLON,true);
		$criteria->compare('MDELMUN',$this->MDELMUN,true);
		$criteria->compare('MCIUDAD',$this->MCIUDAD,true);
		$criteria->compare('MCODEST',$this->MCODEST,true);
		$criteria->compare('MCODPOS',$this->MCODPOS,true);
		$criteria->compare('MGLN',$this->MGLN,true);
		$criteria->compare('MOBSERV',$this->MOBSERV,true);
		$criteria->compare('MCVEMON',$this->MCVEMON,true);
		$criteria->compare('MTIPOCA',$this->MTIPOCA,true);
		$criteria->compare('MPEDCTE',$this->MPEDCTE,true);
		$criteria->compare('MLAB',$this->MLAB,true);
		$criteria->compare('MFLETE',$this->MFLETE,true);
		$criteria->compare('MEMBARQ',$this->MEMBARQ,true);
		$criteria->compare('MTALON',$this->MTALON,true);
		$criteria->compare('MPESON',$this->MPESON,true);
		$criteria->compare('MPESOB',$this->MPESOB,true);
		$criteria->compare('MIMPFLE',$this->MIMPFLE,true);
		$criteria->compare('MIMPOTR',$this->MIMPOTR,true);
		$criteria->compare('MLISPRE',$this->MLISPRE,true);
		$criteria->compare('MPREESP',$this->MPREESP,true);
		$criteria->compare('MIVAINC',$this->MIVAINC);
		$criteria->compare('MIEPSINC',$this->MIEPSINC);
		$criteria->compare('MIVAREN',$this->MIVAREN);
		$criteria->compare('MIVARENR',$this->MIVARENR);
		$criteria->compare('MIEPSREN',$this->MIEPSREN);
		$criteria->compare('MVIGIM',$this->MVIGIM);
		$criteria->compare('MIMPBRU',$this->MIMPBRU,true);
		$criteria->compare('MIMPDES',$this->MIMPDES,true);
		$criteria->compare('MDESREN',$this->MDESREN,true);
		$criteria->compare('MIMPCOS',$this->MIMPCOS,true);
		$criteria->compare('MIMPIVA',$this->MIMPIVA,true);
		$criteria->compare('MIMPIVAR',$this->MIMPIVAR,true);
		$criteria->compare('MIMPISRR',$this->MIMPISRR,true);
		$criteria->compare('MIMPIEPS',$this->MIMPIEPS,true);
		$criteria->compare('MCOMAGE',$this->MCOMAGE,true);
		$criteria->compare('MINICIA',$this->MINICIA,true);
		$criteria->compare('MINIAUT',$this->MINIAUT,true);
		$criteria->compare('MCVECOT',$this->MCVECOT,true);
		$criteria->compare('MNUMCOT',$this->MNUMCOT,true);
		$criteria->compare('MYEARAPRO',$this->MYEARAPRO,true);
		$criteria->compare('MNUMCER',$this->MNUMCER,true);
		$criteria->compare('MSELLO',$this->MSELLO,true);
		$criteria->compare('MCADORIG',$this->MCADORIG,true);
		$criteria->compare('MUUID',$this->MUUID,true);
		$criteria->compare('MFECTIMBRE',$this->MFECTIMBRE,true);
		$criteria->compare('MNUMCERSAT',$this->MNUMCERSAT,true);
		$criteria->compare('MSELLOSAT',$this->MSELLOSAT,true);
		$criteria->compare('MXML',$this->MXML,true);
		$criteria->compare('MENVIADO',$this->MENVIADO);
		$criteria->compare('MTIPOPAGO',$this->MTIPOPAGO,true);
		$criteria->compare('MNUMCTAPAG',$this->MNUMCTAPAG,true);
		$criteria->compare('MPARCIAL',$this->MPARCIAL);
		$criteria->compare('MBAJA',$this->MBAJA);
		$criteria->compare('MCVEFAC',$this->MCVEFAC,true);
		$criteria->compare('MNUMFAC',$this->MNUMFAC,true);
		$criteria->compare('MTIPOPE',$this->MTIPOPE,true);
		$criteria->compare('MCVEPEDIM',$this->MCVEPEDIM,true);
		$criteria->compare('MCERORI',$this->MCERORI);
		$criteria->compare('MSUBDIV',$this->MSUBDIV);
		$criteria->compare('MNUMCERORI',$this->MNUMCERORI,true);
		$criteria->compare('MCVEINC',$this->MCVEINC,true);
		$criteria->compare('MNEXPCONFI',$this->MNEXPCONFI,true);
		$criteria->compare('MTIPOCAEXP',$this->MTIPOCAEXP,true);
		$criteria->compare('MTOTUSD',$this->MTOTUSD,true);
		$criteria->compare('MOBSEXP',$this->MOBSEXP,true);
		$criteria->compare('MCONFSAT',$this->MCONFSAT,true);
		$criteria->compare('MMETPAG',$this->MMETPAG,true);
		$criteria->compare('MUCFDI',$this->MUCFDI,true);
		$criteria->compare('MCVEREL',$this->MCVEREL,true);
		$criteria->compare('MFOLREL',$this->MFOLREL,true);
		$criteria->compare('MTIPOREL',$this->MTIPOREL,true);
		$criteria->compare('MUUIDREL',$this->MUUIDREL,true);
		$criteria->compare('MPORLOC',$this->MPORLOC,true);
		$criteria->compare('MLOCINC',$this->MLOCINC);
		$criteria->compare('MLOCREN',$this->MLOCREN);
		$criteria->compare('MPORLOCR',$this->MPORLOCR,true);
		$criteria->compare('MIMPIEPSC',$this->MIMPIEPSC,true);
		$criteria->compare('MIMPLOC',$this->MIMPLOC,true);
		$criteria->compare('MIMPLOCR',$this->MIMPLOCR,true);
		$criteria->compare('MDESCBD',$this->MDESCBD,true);
		$criteria->compare('MCVEPSBD',$this->MCVEPSBD,true);
		$criteria->compare('MMEDSATBD',$this->MMEDSATBD,true);
		$criteria->compare('MVTAGLO',$this->MVTAGLO);
		$criteria->compare('MNOREP',$this->MNOREP);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return FTFACTG the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
