<?php

/**
 * This is the model class for table "SACLAVE".
 *
 * The followings are the available columns in table 'SACLAVE':
 * @property string $PNUMERO
 * @property string $PDESCR
 * @property string $PDESCOR
 * @property string $PNUMSUC
 * @property string $PFORMA
 * @property string $PNATURA
 * @property string $PENOSAL
 * @property string $PSIGNO
 * @property string $PMODULO
 * @property string $PTIPOPOL
 * @property string $PCVEMON
 * @property string $POBSERV
 * @property string $PLAB
 * @property string $PFLETE
 * @property string $PEMBARQ
 * @property string $PCVEIVA
 * @property string $PCVEIVAR
 * @property string $PCVEISRR
 * @property string $PCVEIEPS
 * @property boolean $PIVAINC
 * @property boolean $PIEPSINC
 * @property boolean $PIVAREN
 * @property boolean $PIVARENR
 * @property boolean $PIEPSREN
 * @property string $PFOLPRI
 * @property string $PFOLIO
 * @property string $PSERIE
 * @property string $PULTFOL
 * @property string $PNUMAPRO
 * @property string $PLISPRE
 * @property boolean $PPREANT
 * @property boolean $PVIGIM
 * @property boolean $PNOCANT
 * @property string $PNUMCOP
 * @property boolean $PTRASPA
 * @property boolean $PCFDI
 * @property string $PFECAPRO
 * @property string $PLLAPRIV
 * @property string $PPASSWD
 * @property string $PNUMCER
 * @property string $PCERTIF
 * @property string $PFECFINCER
 * @property string $PHORFINCER
 * @property boolean $PLUGEXP
 * @property string $PCALLE
 * @property string $PNUMEXT
 * @property string $PNUMINT
 * @property string $PREFEREN
 * @property string $PCOLON
 * @property string $PDELMUN
 * @property string $PCIUDAD
 * @property string $PCODEST
 * @property string $PPAIS
 * @property string $PCODPOS
 * @property string $PREGFIS
 * @property boolean $PPARCIAL
 * @property boolean $PFACPAGCOM
 * @property string $PCTACARTE
 * @property boolean $PBAJA
 * @property string $PCTAVTA
 * @property string $PNUMCENVTA
 * @property string $PCTACTO
 * @property string $PNUMCENCTO
 * @property boolean $PVIGIM2
 * @property string $PNUMCOP2
 * @property string $PFORMA2
 * @property boolean $PINVFOL
 * @property string $PCVEUSO
 * @property string $PPORLOC
 * @property boolean $PLOCINC
 * @property boolean $PLOCREN
 * @property string $PPORLOCR
 * @property string $PCODPROFG
 * @property string $PCVECREFG
 * @property boolean $PTOTBD
 * @property string $PDESCBD
 * @property string $PCVEPSBD
 * @property string $PMEDSATBD
 */
class SACLAVE extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'SACLAVE';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('PNUMERO, PFORMA, PSIGNO, PTIPOPOL, PCVEIVA, PCVEIVAR, PCVEIEPS, PIVAINC, PIEPSINC, PIVAREN, PIVARENR, PIEPSREN, PSERIE, PLISPRE, PPREANT, PVIGIM, PNOCANT, PTRASPA, PCFDI, PLUGEXP, PCODPOS, PPARCIAL, PFACPAGCOM, PBAJA, PVIGIM2, PNUMCOP2, PINVFOL, PPORLOC, PLOCINC, PLOCREN, PPORLOCR, PCODPROFG, PCVECREFG, PTOTBD', 'required'),
			array('PNUMERO, PNUMSUC, PFORMA, PFLETE, PFOLPRI, PFOLIO, PULTFOL, PNUMCENVTA, PNUMCENCTO, PFORMA2, PCVECREFG', 'length', 'max'=>6),
			array('PDESCR, PCOLON, PDELMUN, PCIUDAD', 'length', 'max'=>30),
			array('PDESCOR, PHORFINCER', 'length', 'max'=>8),
			array('PNATURA, PENOSAL, PMODULO, PTIPOPOL, PNUMCOP, PNUMCOP2', 'length', 'max'=>1),
			array('PSIGNO, PCVEIVA, PCVEISRR', 'length', 'max'=>2),
			array('PCVEMON, PSERIE, PCODEST, PREGFIS, PCVEUSO, PMEDSATBD', 'length', 'max'=>3),
			array('POBSERV, PLAB', 'length', 'max'=>15),
			array('PEMBARQ, PPAIS', 'length', 'max'=>24),
			array('PCVEIVAR', 'length', 'max'=>12),
			array('PCVEIEPS, PPORLOC, PPORLOCR', 'length', 'max'=>5),
			array('PNUMAPRO, PNUMEXT, PNUMINT, PCODPOS, PCVEPSBD', 'length', 'max'=>10),
			array('PLISPRE', 'length', 'max'=>4),
			array('PLLAPRIV, PCERTIF', 'length', 'max'=>2500),
			array('PPASSWD, PCALLE, PREFEREN', 'length', 'max'=>50),
			array('PNUMCER, PCTACARTE, PCTAVTA, PCTACTO, PCODPROFG', 'length', 'max'=>20),
			array('PDESCBD', 'length', 'max'=>40),
			array('PFECAPRO, PFECFINCER', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('PNUMERO, PDESCR, PDESCOR, PNUMSUC, PFORMA, PNATURA, PENOSAL, PSIGNO, PMODULO, PTIPOPOL, PCVEMON, POBSERV, PLAB, PFLETE, PEMBARQ, PCVEIVA, PCVEIVAR, PCVEISRR, PCVEIEPS, PIVAINC, PIEPSINC, PIVAREN, PIVARENR, PIEPSREN, PFOLPRI, PFOLIO, PSERIE, PULTFOL, PNUMAPRO, PLISPRE, PPREANT, PVIGIM, PNOCANT, PNUMCOP, PTRASPA, PCFDI, PFECAPRO, PLLAPRIV, PPASSWD, PNUMCER, PCERTIF, PFECFINCER, PHORFINCER, PLUGEXP, PCALLE, PNUMEXT, PNUMINT, PREFEREN, PCOLON, PDELMUN, PCIUDAD, PCODEST, PPAIS, PCODPOS, PREGFIS, PPARCIAL, PFACPAGCOM, PCTACARTE, PBAJA, PCTAVTA, PNUMCENVTA, PCTACTO, PNUMCENCTO, PVIGIM2, PNUMCOP2, PFORMA2, PINVFOL, PCVEUSO, PPORLOC, PLOCINC, PLOCREN, PPORLOCR, PCODPROFG, PCVECREFG, PTOTBD, PDESCBD, PCVEPSBD, PMEDSATBD', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'PNUMERO' => 'Pnumero',
			'PDESCR' => 'Pdescr',
			'PDESCOR' => 'Pdescor',
			'PNUMSUC' => 'Pnumsuc',
			'PFORMA' => 'Pforma',
			'PNATURA' => 'Pnatura',
			'PENOSAL' => 'Penosal',
			'PSIGNO' => 'Psigno',
			'PMODULO' => 'Pmodulo',
			'PTIPOPOL' => 'Ptipopol',
			'PCVEMON' => 'Pcvemon',
			'POBSERV' => 'Pobserv',
			'PLAB' => 'Plab',
			'PFLETE' => 'Pflete',
			'PEMBARQ' => 'Pembarq',
			'PCVEIVA' => 'Pcveiva',
			'PCVEIVAR' => 'Pcveivar',
			'PCVEISRR' => 'Pcveisrr',
			'PCVEIEPS' => 'Pcveieps',
			'PIVAINC' => 'Pivainc',
			'PIEPSINC' => 'Piepsinc',
			'PIVAREN' => 'Pivaren',
			'PIVARENR' => 'Pivarenr',
			'PIEPSREN' => 'Piepsren',
			'PFOLPRI' => 'Pfolpri',
			'PFOLIO' => 'Pfolio',
			'PSERIE' => 'Pserie',
			'PULTFOL' => 'Pultfol',
			'PNUMAPRO' => 'Pnumapro',
			'PLISPRE' => 'Plispre',
			'PPREANT' => 'Ppreant',
			'PVIGIM' => 'Pvigim',
			'PNOCANT' => 'Pnocant',
			'PNUMCOP' => 'Pnumcop',
			'PTRASPA' => 'Ptraspa',
			'PCFDI' => 'Pcfdi',
			'PFECAPRO' => 'Pfecapro',
			'PLLAPRIV' => 'Pllapriv',
			'PPASSWD' => 'Ppasswd',
			'PNUMCER' => 'Pnumcer',
			'PCERTIF' => 'Pcertif',
			'PFECFINCER' => 'Pfecfincer',
			'PHORFINCER' => 'Phorfincer',
			'PLUGEXP' => 'Plugexp',
			'PCALLE' => 'Pcalle',
			'PNUMEXT' => 'Pnumext',
			'PNUMINT' => 'Pnumint',
			'PREFEREN' => 'Preferen',
			'PCOLON' => 'Pcolon',
			'PDELMUN' => 'Pdelmun',
			'PCIUDAD' => 'Pciudad',
			'PCODEST' => 'Pcodest',
			'PPAIS' => 'Ppais',
			'PCODPOS' => 'Pcodpos',
			'PREGFIS' => 'Pregfis',
			'PPARCIAL' => 'Pparcial',
			'PFACPAGCOM' => 'Pfacpagcom',
			'PCTACARTE' => 'Pctacarte',
			'PBAJA' => 'Pbaja',
			'PCTAVTA' => 'Pctavta',
			'PNUMCENVTA' => 'Pnumcenvta',
			'PCTACTO' => 'Pctacto',
			'PNUMCENCTO' => 'Pnumcencto',
			'PVIGIM2' => 'Pvigim2',
			'PNUMCOP2' => 'Pnumcop2',
			'PFORMA2' => 'Pforma2',
			'PINVFOL' => 'Pinvfol',
			'PCVEUSO' => 'Pcveuso',
			'PPORLOC' => 'Pporloc',
			'PLOCINC' => 'Plocinc',
			'PLOCREN' => 'Plocren',
			'PPORLOCR' => 'Pporlocr',
			'PCODPROFG' => 'Pcodprofg',
			'PCVECREFG' => 'Pcvecrefg',
			'PTOTBD' => 'Ptotbd',
			'PDESCBD' => 'Pdescbd',
			'PCVEPSBD' => 'Pcvepsbd',
			'PMEDSATBD' => 'Pmedsatbd',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('PNUMERO',$this->PNUMERO,true);
		$criteria->compare('PDESCR',$this->PDESCR,true);
		$criteria->compare('PDESCOR',$this->PDESCOR,true);
		$criteria->compare('PNUMSUC',$this->PNUMSUC,true);
		$criteria->compare('PFORMA',$this->PFORMA,true);
		$criteria->compare('PNATURA',$this->PNATURA,true);
		$criteria->compare('PENOSAL',$this->PENOSAL,true);
		$criteria->compare('PSIGNO',$this->PSIGNO,true);
		$criteria->compare('PMODULO',$this->PMODULO,true);
		$criteria->compare('PTIPOPOL',$this->PTIPOPOL,true);
		$criteria->compare('PCVEMON',$this->PCVEMON,true);
		$criteria->compare('POBSERV',$this->POBSERV,true);
		$criteria->compare('PLAB',$this->PLAB,true);
		$criteria->compare('PFLETE',$this->PFLETE,true);
		$criteria->compare('PEMBARQ',$this->PEMBARQ,true);
		$criteria->compare('PCVEIVA',$this->PCVEIVA,true);
		$criteria->compare('PCVEIVAR',$this->PCVEIVAR,true);
		$criteria->compare('PCVEISRR',$this->PCVEISRR,true);
		$criteria->compare('PCVEIEPS',$this->PCVEIEPS,true);
		$criteria->compare('PIVAINC',$this->PIVAINC);
		$criteria->compare('PIEPSINC',$this->PIEPSINC);
		$criteria->compare('PIVAREN',$this->PIVAREN);
		$criteria->compare('PIVARENR',$this->PIVARENR);
		$criteria->compare('PIEPSREN',$this->PIEPSREN);
		$criteria->compare('PFOLPRI',$this->PFOLPRI,true);
		$criteria->compare('PFOLIO',$this->PFOLIO,true);
		$criteria->compare('PSERIE',$this->PSERIE,true);
		$criteria->compare('PULTFOL',$this->PULTFOL,true);
		$criteria->compare('PNUMAPRO',$this->PNUMAPRO,true);
		$criteria->compare('PLISPRE',$this->PLISPRE,true);
		$criteria->compare('PPREANT',$this->PPREANT);
		$criteria->compare('PVIGIM',$this->PVIGIM);
		$criteria->compare('PNOCANT',$this->PNOCANT);
		$criteria->compare('PNUMCOP',$this->PNUMCOP,true);
		$criteria->compare('PTRASPA',$this->PTRASPA);
		$criteria->compare('PCFDI',$this->PCFDI);
		$criteria->compare('PFECAPRO',$this->PFECAPRO,true);
		$criteria->compare('PLLAPRIV',$this->PLLAPRIV,true);
		$criteria->compare('PPASSWD',$this->PPASSWD,true);
		$criteria->compare('PNUMCER',$this->PNUMCER,true);
		$criteria->compare('PCERTIF',$this->PCERTIF,true);
		$criteria->compare('PFECFINCER',$this->PFECFINCER,true);
		$criteria->compare('PHORFINCER',$this->PHORFINCER,true);
		$criteria->compare('PLUGEXP',$this->PLUGEXP);
		$criteria->compare('PCALLE',$this->PCALLE,true);
		$criteria->compare('PNUMEXT',$this->PNUMEXT,true);
		$criteria->compare('PNUMINT',$this->PNUMINT,true);
		$criteria->compare('PREFEREN',$this->PREFEREN,true);
		$criteria->compare('PCOLON',$this->PCOLON,true);
		$criteria->compare('PDELMUN',$this->PDELMUN,true);
		$criteria->compare('PCIUDAD',$this->PCIUDAD,true);
		$criteria->compare('PCODEST',$this->PCODEST,true);
		$criteria->compare('PPAIS',$this->PPAIS,true);
		$criteria->compare('PCODPOS',$this->PCODPOS,true);
		$criteria->compare('PREGFIS',$this->PREGFIS,true);
		$criteria->compare('PPARCIAL',$this->PPARCIAL);
		$criteria->compare('PFACPAGCOM',$this->PFACPAGCOM);
		$criteria->compare('PCTACARTE',$this->PCTACARTE,true);
		$criteria->compare('PBAJA',$this->PBAJA);
		$criteria->compare('PCTAVTA',$this->PCTAVTA,true);
		$criteria->compare('PNUMCENVTA',$this->PNUMCENVTA,true);
		$criteria->compare('PCTACTO',$this->PCTACTO,true);
		$criteria->compare('PNUMCENCTO',$this->PNUMCENCTO,true);
		$criteria->compare('PVIGIM2',$this->PVIGIM2);
		$criteria->compare('PNUMCOP2',$this->PNUMCOP2,true);
		$criteria->compare('PFORMA2',$this->PFORMA2,true);
		$criteria->compare('PINVFOL',$this->PINVFOL);
		$criteria->compare('PCVEUSO',$this->PCVEUSO,true);
		$criteria->compare('PPORLOC',$this->PPORLOC,true);
		$criteria->compare('PLOCINC',$this->PLOCINC);
		$criteria->compare('PLOCREN',$this->PLOCREN);
		$criteria->compare('PPORLOCR',$this->PPORLOCR,true);
		$criteria->compare('PCODPROFG',$this->PCODPROFG,true);
		$criteria->compare('PCVECREFG',$this->PCVECREFG,true);
		$criteria->compare('PTOTBD',$this->PTOTBD);
		$criteria->compare('PDESCBD',$this->PDESCBD,true);
		$criteria->compare('PCVEPSBD',$this->PCVEPSBD,true);
		$criteria->compare('PMEDSATBD',$this->PMEDSATBD,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SACLAVE the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
