<?php

/**
 * This is the model class for table "IBINVEN".
 *
 * The followings are the available columns in table 'IBINVEN':
 * @property string $ACODPRO
 * @property string $AGRUPO
 * @property string $ADESCR
 * @property string $AUNIME
 * @property string $ACODPRV
 * @property string $AEAN13
 * @property string $ADUN14
 * @property string $APORIVA
 * @property string $APORIVAR
 * @property string $APORIEPS
 * @property boolean $AESINVE
 * @property string $ASTOCK
 * @property string $APORDES
 * @property string $AFECHAU
 * @property string $AMANSER
 * @property string $ACTAPREDI
 * @property boolean $ABAJA
 * @property string $AOBSERV
 * @property string $AFRAARA
 * @property string $AFACMEDADU
 * @property string $AMARCA
 * @property string $AMODELO
 * @property string $ACVEPS
 * @property boolean $AEXEIVA
 * @property string $ACUOIEPS
 * @property string $APORLOC
 */
class IBINVEN extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'IBINVEN';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ACODPRO, AGRUPO, ADESCR, AUNIME, APORIVA, APORIVAR, APORIEPS, AESINVE, APORDES, ABAJA, AEXEIVA, ACUOIEPS, APORLOC', 'required'),
			array('ACODPRO, ACODPRV', 'length', 'max'=>20),
			array('AGRUPO', 'length', 'max'=>4),
			array('AUNIME', 'length', 'max'=>3),
			array('AEAN13, ADUN14', 'length', 'max'=>16),
			array('APORIVA', 'length', 'max'=>2),
			array('APORIVAR', 'length', 'max'=>12),
			array('APORIEPS, APORDES, APORLOC', 'length', 'max'=>5),
			array('ASTOCK, AMANSER', 'length', 'max'=>1),
			array('ACTAPREDI', 'length', 'max'=>30),
			array('AFRAARA, ACVEPS', 'length', 'max'=>10),
			array('AFACMEDADU', 'length', 'max'=>9),
			array('AMARCA', 'length', 'max'=>35),
			array('AMODELO', 'length', 'max'=>80),
			array('ACUOIEPS', 'length', 'max'=>7),
			array('AFECHAU, AOBSERV', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ACODPRO, AGRUPO, ADESCR, AUNIME, ACODPRV, AEAN13, ADUN14, APORIVA, APORIVAR, APORIEPS, AESINVE, ASTOCK, APORDES, AFECHAU, AMANSER, ACTAPREDI, ABAJA, AOBSERV, AFRAARA, AFACMEDADU, AMARCA, AMODELO, ACVEPS, AEXEIVA, ACUOIEPS, APORLOC', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ACODPRO' => 'Acodpro',
			'AGRUPO' => 'Agrupo',
			'ADESCR' => 'Adescr',
			'AUNIME' => 'Aunime',
			'ACODPRV' => 'Acodprv',
			'AEAN13' => 'Aean13',
			'ADUN14' => 'Adun14',
			'APORIVA' => 'Aporiva',
			'APORIVAR' => 'Aporivar',
			'APORIEPS' => 'Aporieps',
			'AESINVE' => 'Aesinve',
			'ASTOCK' => 'Astock',
			'APORDES' => 'Apordes',
			'AFECHAU' => 'Afechau',
			'AMANSER' => 'Amanser',
			'ACTAPREDI' => 'Actapredi',
			'ABAJA' => 'Abaja',
			'AOBSERV' => 'Aobserv',
			'AFRAARA' => 'Afraara',
			'AFACMEDADU' => 'Afacmedadu',
			'AMARCA' => 'Amarca',
			'AMODELO' => 'Amodelo',
			'ACVEPS' => 'Acveps',
			'AEXEIVA' => 'Aexeiva',
			'ACUOIEPS' => 'Acuoieps',
			'APORLOC' => 'Aporloc',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ACODPRO',$this->ACODPRO,true);
		$criteria->compare('AGRUPO',$this->AGRUPO,true);
		$criteria->compare('ADESCR',$this->ADESCR,true);
		$criteria->compare('AUNIME',$this->AUNIME,true);
		$criteria->compare('ACODPRV',$this->ACODPRV,true);
		$criteria->compare('AEAN13',$this->AEAN13,true);
		$criteria->compare('ADUN14',$this->ADUN14,true);
		$criteria->compare('APORIVA',$this->APORIVA,true);
		$criteria->compare('APORIVAR',$this->APORIVAR,true);
		$criteria->compare('APORIEPS',$this->APORIEPS,true);
		$criteria->compare('AESINVE',$this->AESINVE);
		$criteria->compare('ASTOCK',$this->ASTOCK,true);
		$criteria->compare('APORDES',$this->APORDES,true);
		$criteria->compare('AFECHAU',$this->AFECHAU,true);
		$criteria->compare('AMANSER',$this->AMANSER,true);
		$criteria->compare('ACTAPREDI',$this->ACTAPREDI,true);
		$criteria->compare('ABAJA',$this->ABAJA);
		$criteria->compare('AOBSERV',$this->AOBSERV,true);
		$criteria->compare('AFRAARA',$this->AFRAARA,true);
		$criteria->compare('AFACMEDADU',$this->AFACMEDADU,true);
		$criteria->compare('AMARCA',$this->AMARCA,true);
		$criteria->compare('AMODELO',$this->AMODELO,true);
		$criteria->compare('ACVEPS',$this->ACVEPS,true);
		$criteria->compare('AEXEIVA',$this->AEXEIVA);
		$criteria->compare('ACUOIEPS',$this->ACUOIEPS,true);
		$criteria->compare('APORLOC',$this->APORLOC,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return IBINVEN the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
