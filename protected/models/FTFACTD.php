<?php

/**
 * This is the model class for table "FTFACTD".
 *
 * The followings are the available columns in table 'FTFACTD':
 * @property string $MCVEMOV
 * @property string $MFOLIOG
 * @property string $MNUMREN
 * @property string $MALMACE
 * @property string $MCODPRO
 * @property string $MGRUPO
 * @property string $MDESCR
 * @property string $MUNIMED
 * @property string $MCANTID
 * @property string $MPRECIO
 * @property string $MIMPCOS
 * @property string $MPORIVA
 * @property string $MPORIVAR
 * @property string $MPORIEPS
 * @property string $MPORIEPSG
 * @property string $MPORDES
 * @property string $MPARTID
 * @property string $MDIASE
 * @property string $MFECHAE
 * @property string $MPEDIM
 * @property string $MFECPED
 * @property string $MADUANA
 * @property string $MCVEPED
 * @property string $MNUMPED
 * @property string $MRENPED
 * @property string $MNUMTIC
 * @property boolean $MEXEIVA
 * @property string $MPORLOC
 * @property string $MPORLOCR
 * @property string $MCUOIEPS
 * @property string $MIMPORT
 * @property string $MIMPDESR
 * @property string $MIMPDE1
 * @property string $MIMPDE2
 * @property string $MIMPDE3
 * @property string $MIMPNETO
 * @property string $MIMPIVA
 * @property string $MIMPIVAR
 * @property string $MIMPIEPS
 * @property string $MIMPIEPSC
 * @property string $MIMPISRR
 */
class FTFACTD extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'FTFACTD';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('MCVEMOV, MFOLIOG, MNUMREN, MALMACE, MCANTID, MPRECIO, MIMPCOS, MPORIVA, MPORIVAR, MPORIEPS, MPORIEPSG, MPORDES, MEXEIVA, MPORLOC, MPORLOCR, MCUOIEPS, MIMPORT, MIMPDESR, MIMPDE1, MIMPDE2, MIMPDE3, MIMPNETO, MIMPIVA, MIMPIVAR, MIMPIEPS, MIMPIEPSC, MIMPISRR', 'required'),
			array('MCVEMOV, MFOLIOG, MPORIEPSG, MCVEPED, MNUMPED', 'length', 'max'=>6),
			array('MNUMREN, MRENPED', 'length', 'max'=>10),
			array('MALMACE, MUNIMED, MDIASE', 'length', 'max'=>3),
			array('MCODPRO', 'length', 'max'=>20),
			array('MGRUPO', 'length', 'max'=>4),
			array('MCANTID', 'length', 'max'=>9),
			array('MPRECIO, MPORIVAR', 'length', 'max'=>12),
			array('MIMPCOS, MPEDIM, MIMPORT, MIMPDESR, MIMPDE1, MIMPDE2, MIMPDE3, MIMPNETO, MIMPIVA, MIMPIVAR, MIMPIEPS, MIMPIEPSC, MIMPISRR', 'length', 'max'=>15),
			array('MPORIVA', 'length', 'max'=>2),
			array('MPORIEPS, MPORDES, MPORLOC, MPORLOCR', 'length', 'max'=>5),
			array('MPARTID', 'length', 'max'=>8),
			array('MADUANA', 'length', 'max'=>50),
			array('MNUMTIC', 'length', 'max'=>16),
			array('MCUOIEPS', 'length', 'max'=>7),
			array('MDESCR, MFECHAE, MFECPED', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('MCVEMOV, MFOLIOG, MNUMREN, MALMACE, MCODPRO, MGRUPO, MDESCR, MUNIMED, MCANTID, MPRECIO, MIMPCOS, MPORIVA, MPORIVAR, MPORIEPS, MPORIEPSG, MPORDES, MPARTID, MDIASE, MFECHAE, MPEDIM, MFECPED, MADUANA, MCVEPED, MNUMPED, MRENPED, MNUMTIC, MEXEIVA, MPORLOC, MPORLOCR, MCUOIEPS, MIMPORT, MIMPDESR, MIMPDE1, MIMPDE2, MIMPDE3, MIMPNETO, MIMPIVA, MIMPIVAR, MIMPIEPS, MIMPIEPSC, MIMPISRR', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'MCVEMOV' => 'Mcvemov',
			'MFOLIOG' => 'Mfoliog',
			'MNUMREN' => 'Mnumren',
			'MALMACE' => 'Malmace',
			'MCODPRO' => 'Mcodpro',
			'MGRUPO' => 'Mgrupo',
			'MDESCR' => 'Mdescr',
			'MUNIMED' => 'Munimed',
			'MCANTID' => 'Mcantid',
			'MPRECIO' => 'Mprecio',
			'MIMPCOS' => 'Mimpcos',
			'MPORIVA' => 'Mporiva',
			'MPORIVAR' => 'Mporivar',
			'MPORIEPS' => 'Mporieps',
			'MPORIEPSG' => 'Mporiepsg',
			'MPORDES' => 'Mpordes',
			'MPARTID' => 'Mpartid',
			'MDIASE' => 'Mdiase',
			'MFECHAE' => 'Mfechae',
			'MPEDIM' => 'Mpedim',
			'MFECPED' => 'Mfecped',
			'MADUANA' => 'Maduana',
			'MCVEPED' => 'Mcveped',
			'MNUMPED' => 'Mnumped',
			'MRENPED' => 'Mrenped',
			'MNUMTIC' => 'Mnumtic',
			'MEXEIVA' => 'Mexeiva',
			'MPORLOC' => 'Mporloc',
			'MPORLOCR' => 'Mporlocr',
			'MCUOIEPS' => 'Mcuoieps',
			'MIMPORT' => 'Mimport',
			'MIMPDESR' => 'Mimpdesr',
			'MIMPDE1' => 'Mimpde1',
			'MIMPDE2' => 'Mimpde2',
			'MIMPDE3' => 'Mimpde3',
			'MIMPNETO' => 'Mimpneto',
			'MIMPIVA' => 'Mimpiva',
			'MIMPIVAR' => 'Mimpivar',
			'MIMPIEPS' => 'Mimpieps',
			'MIMPIEPSC' => 'Mimpiepsc',
			'MIMPISRR' => 'Mimpisrr',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('MCVEMOV',$this->MCVEMOV,true);
		$criteria->compare('MFOLIOG',$this->MFOLIOG,true);
		$criteria->compare('MNUMREN',$this->MNUMREN,true);
		$criteria->compare('MALMACE',$this->MALMACE,true);
		$criteria->compare('MCODPRO',$this->MCODPRO,true);
		$criteria->compare('MGRUPO',$this->MGRUPO,true);
		$criteria->compare('MDESCR',$this->MDESCR,true);
		$criteria->compare('MUNIMED',$this->MUNIMED,true);
		$criteria->compare('MCANTID',$this->MCANTID,true);
		$criteria->compare('MPRECIO',$this->MPRECIO,true);
		$criteria->compare('MIMPCOS',$this->MIMPCOS,true);
		$criteria->compare('MPORIVA',$this->MPORIVA,true);
		$criteria->compare('MPORIVAR',$this->MPORIVAR,true);
		$criteria->compare('MPORIEPS',$this->MPORIEPS,true);
		$criteria->compare('MPORIEPSG',$this->MPORIEPSG,true);
		$criteria->compare('MPORDES',$this->MPORDES,true);
		$criteria->compare('MPARTID',$this->MPARTID,true);
		$criteria->compare('MDIASE',$this->MDIASE,true);
		$criteria->compare('MFECHAE',$this->MFECHAE,true);
		$criteria->compare('MPEDIM',$this->MPEDIM,true);
		$criteria->compare('MFECPED',$this->MFECPED,true);
		$criteria->compare('MADUANA',$this->MADUANA,true);
		$criteria->compare('MCVEPED',$this->MCVEPED,true);
		$criteria->compare('MNUMPED',$this->MNUMPED,true);
		$criteria->compare('MRENPED',$this->MRENPED,true);
		$criteria->compare('MNUMTIC',$this->MNUMTIC,true);
		$criteria->compare('MEXEIVA',$this->MEXEIVA);
		$criteria->compare('MPORLOC',$this->MPORLOC,true);
		$criteria->compare('MPORLOCR',$this->MPORLOCR,true);
		$criteria->compare('MCUOIEPS',$this->MCUOIEPS,true);
		$criteria->compare('MIMPORT',$this->MIMPORT,true);
		$criteria->compare('MIMPDESR',$this->MIMPDESR,true);
		$criteria->compare('MIMPDE1',$this->MIMPDE1,true);
		$criteria->compare('MIMPDE2',$this->MIMPDE2,true);
		$criteria->compare('MIMPDE3',$this->MIMPDE3,true);
		$criteria->compare('MIMPNETO',$this->MIMPNETO,true);
		$criteria->compare('MIMPIVA',$this->MIMPIVA,true);
		$criteria->compare('MIMPIVAR',$this->MIMPIVAR,true);
		$criteria->compare('MIMPIEPS',$this->MIMPIEPS,true);
		$criteria->compare('MIMPIEPSC',$this->MIMPIEPSC,true);
		$criteria->compare('MIMPISRR',$this->MIMPISRR,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return FTFACTD the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
